from django.db import models
from axis.models import MyUser


class Test(models.Model):
    name = models.CharField(max_length=50, null=False, default="Round")
    start = models.DateTimeField()
    end = models.DateTimeField()
    conduct_this_test = models.BooleanField(default=True)
    duration = models.DurationField()

    def __str__(self):
        return str(self.name)


class Question(models.Model):
    A = 'A'
    B = 'B'
    C = 'C'
    D = 'D'

    ANSWER_CHOICE = (
        (A, 'A'),
        (B, 'B'),
        (C, 'C'),
        (D, 'D'),
    )

    test = models.ForeignKey(Test, null=False, on_delete=models.CASCADE)
    image = models.FileField(null=True, upload_to="turboflux_test_images/", blank=True)
    text = models.TextField(max_length=5000, null=False, blank=True)
    answer = models.CharField(max_length=2, null=False, choices=ANSWER_CHOICE, default=A)
    marks = models.FloatField(null=False, default=0)
    negative_marks = models.FloatField(null=False, default=0)

    def __str__(self):
        return str(self.id) + ": " + self.text


class TurbofluxUserTestMetadata(models.Model):
    questions_answered = models.ManyToManyField(Question, blank=True)
    user = models.ForeignKey(MyUser, null=False, on_delete=models.CASCADE)
    test = models.ForeignKey(Test, null=False, on_delete=models.CASCADE)
    score = models.FloatField(default=0)
    last_question_id = models.IntegerField(null=True, blank=True)
    started_at = models.DateTimeField(auto_now_add=True)

    def update_answer(self, question_id: int, answer):
        question = Question.objects.get(id=question_id)

        try:
            self.questions_answered.get(id=question_id)
            return
        except Question.DoesNotExist:
            self.questions_answered.add(question)

        if question.answer == answer:
            self.score += question.marks
        else:
            self.score -= question.negative_marks

        self.save()

    def __str__(self):
        return "Round " + str(self.test.name) + " : " + str(self.user.f_name) + " - " + str(self.questions_answered)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None, last_question_id=None):
        if last_question_id is None:
            self.last_question_id = self.test.question_set.all().order_by('id')[0].id
        else:
            q = Question.objects.get(id=last_question_id)
            if q.test == self.test:
                self.last_question_id = last_question_id

        super().save()

