from django.contrib import admin
from django.db import models
from django.forms.widgets import Textarea
from .models import Test, Question, TurbofluxUserTestMetadata


@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    list_display = ('name', 'start', 'end',)
    list_filter = ()

    search_fields = ('text',)
    ordering = ('id',)
    filter_horizontal = ()


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('test', 'text', 'image', 'answer')
    list_filter = ('test',)

    search_fields = ('text', 'test__name',)
    ordering = ('id',)
    filter_horizontal = ()

    formfield_overrides = {
        models.CharField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
    }


@admin.register(TurbofluxUserTestMetadata)
class TurbofluxUserTestMetadataAdmin(admin.ModelAdmin):
    list_display = ('test', 'user', 'score',)
    list_filter = ('test',)

    search_fields = ('test__name', 'user__email', )
    ordering = ('id',)
    filter_horizontal = ('questions_answered',)
    readonly_fields = ('last_question_id', 'started_at',)
