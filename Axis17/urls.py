"""Axis17 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from axis.views import not_found
from django.conf.urls import handler404

urlpatterns = [
    url(r'^axis-admin/', admin.site.urls),
    url(r'^', include('axis.urls', namespace="axis")),
    url(r'^bakerstreet/', include('bakerstreet.urls', namespace="bakerstreet")),
    url(r'^turboflux/', include('turboflux.urls', namespace="turboflux")),
    url(r'^freakomatix/', include('freakomatix.urls', namespace="freakomatix")),
    url(r'^electroblitz/', include('electroblitz.urls', namespace="electroblitz")),
    url(r'^api-auth/', include('rest_framework.urls')),
    # url('', include('social_django.urls', namespace='social'))
    # url(r'^select2/', include('select2.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# urlpatterns += [
#     url(r'^[a-zA-Z]+', not_found),
# ]
#
handler404 = not_found
