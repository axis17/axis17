// Counter logic
function counter() {
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(countdown / (60 * 60 * 24));
    var hours = Math.floor((countdown % (60 * 60 * 24)) / (60 * 60));
    var minutes = Math.floor((countdown % (60 * 60)) / (60));
    var seconds = Math.floor((countdown % (60)));

    // Output the result in an element with id="demo"

    var countdownStr = "Time left : ";

    if(days !== 0)
        countdownStr += (days.toString() + "d ");

    if(hours !== 0)
        countdownStr += (hours.toString() + "h ");

    if(minutes !== 0)
        countdownStr += (minutes.toString() + "m ");

    countdownStr += (seconds.toString() + "s ");

    // document.getElementById("question").innerHTML = countdownStr;
    $('#question').html(countdownStr);
    // console.log(countdownStr)
    // If the count down is over, write some text
    if (countdown <= 0) {
        clearInterval(x);
        location.reload();
    }
    countdown--;
}

counter();
// Update the count down every 1 second
var x = setInterval(counter, 1000);