from django.contrib import admin
from .models import Question, UserQuestionAnswer, FreakomatrixUserTestMetadata, Test


class QuestionAdmin(admin.ModelAdmin):

    list_display = ('test', 'text')
    ordering = ('test',)


class UserQuestionAnswerAdmin(admin.ModelAdmin):

    list_display = ('user', 'question', 'answer',)
    list_filter = ('user',)
    # ordering = ('test', '-questions_solved', 'time_taken')


admin.site.register(Question, QuestionAdmin)
admin.site.register(UserQuestionAnswer, UserQuestionAnswerAdmin)
admin.site.register(FreakomatrixUserTestMetadata)
admin.site.register(Test)
