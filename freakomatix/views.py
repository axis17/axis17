from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, HttpResponse
from axis.views import event_registration_email
from axis.models import MyUser, Event, SingleParticipantEventRegistration
from axis.views import login_signup
from freakomatix.models import FreakomatrixUserTestMetadata, Test, Question, UserQuestionAnswer
from django.contrib import messages
import json
import datetime
from django.utils import timezone

EVENT_ID = 14


def rules(request):
    if request.method == 'GET':
        return render(request, 'freakomatix/freakomatix_rules.html')
    elif request.method == 'POST' and ('Login' in request.POST or 'Signup' in request.POST):
        return login_signup(request, "freakomatix:home")
    elif request.method == 'POST' and 'PLAY' in request.POST:
        # register_for_bakerstreet(request)
        return redirect('freakomatix:home')


# @login_required(login_url='/#login')
def home(request):
    try:
        test = Test.objects.get(start__lte=datetime.datetime.now(), end__gte=datetime.datetime.now())
    except Test.DoesNotExist:
        # TODO : Change this
        return redirect('freakomatix:time_completed')

    if request.user.is_anonymous:
        return redirect('freakomatix:rules')

    test.question_set.all()[0].save()

    # TODO: Register user for turboflux

    # if not is_registered_for_turboflux(request):
    #     return redirect('turboflux:rules')

    if request.method == 'POST':
        user_metadata = FreakomatrixUserTestMetadata.objects.get(test=test, user=request.user)

        if 'submit_answer' in request.POST:
            question_id = request.POST['question_id']
            answer = request.POST['answer']
            # answer = str(answer).upper()
            question = Question.objects.get(id=question_id)
            ans = UserQuestionAnswer.objects.create(answer=answer, question=question, user=request.user)
            ans.save()
            user_metadata.update_answer(question_id, answer)
            messages.info(request, "Answer submitted.")
        elif 'change_question' in request.POST:
            user_metadata.save(last_question_id=int(request.POST['question_id']))

        return redirect('freakomatix:home')

    try:
        user_metadata = FreakomatrixUserTestMetadata.objects.get(user=request.user, test=test)
    except FreakomatrixUserTestMetadata.DoesNotExist:
        user_metadata = FreakomatrixUserTestMetadata.objects.create(test=test, user=request.user)
        user_metadata.save()

    questions = user_metadata.test.question_set.all().order_by('id')
    last_question = Question.objects.get(id=user_metadata.last_question_id)

    attempted_question_ids = []

    countdown = (test.end - timezone.now()).seconds

    if countdown <= 0:
        return redirect('turboflux:time_completed')

    for q in user_metadata.questions_answered.all().only('id'):
        attempted_question_ids.append(q.id)
    return render(request, 'freakomatix/home.html', context={
                                                    'questions': questions,
                                                    'countdown': countdown,
                                                    'last_question': last_question,
                                                    'attempted_question_ids': attempted_question_ids
                                                })


def register_for_bakerstreet(request):
    try:
        SingleParticipantEventRegistration.objects.get(user=request.user, event_id=EVENT_ID)
    except:
        SingleParticipantEventRegistration.objects.create(user=request.user, event_id=EVENT_ID)

    return


# def is_registered_for_turboflux(request):
#     try:
#         SingleParticipantEventRegistration.objects.get(user=request.user, event_id=EVENT_ID)
#         return True
#     except:
#         return False


def time_completed(request):
    try:
        test = Test.objects.get(start__lte=datetime.datetime.now(), end__gte=datetime.datetime.now())
        return redirect('freakomatix:home')
    except Test.DoesNotExist:
            return render(request, 'freakomatix/time_completed.html', context={})


