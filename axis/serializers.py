from rest_framework import serializers
from axis.models import *


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = '__all__'


class EventCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = EventCategory
        fields = '__all__'


class WorkshopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workshop
        fields = '__all__'


class GuestLecturesSerializer(serializers.ModelSerializer):
    class Meta:
        model = GuestLectures
        fields = '__all__'


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = '__all__'


class SocialInitiativeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialInitiative
        fields = '__all__'


class ExhibitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exhibition
        fields = '__all__'


class TalksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Talks
        fields = '__all__'


class TimeLineSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimeLine
        fields = '__all__'


class CurrentUpdatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = CurrentUpdates
        fields = '__all__'


class InaugurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inauguration
        fields = '__all__'


class SummitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Summit
        fields = '__all__'


class TechieNiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = TechieNite
        fields = '__all__'


class MyUserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(required=True, allow_blank=False, max_length=100)
    password = serializers.CharField(required=True, allow_blank=False, max_length=100)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class UserCreationSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField(required=True, max_length=200)
    password2 = serializers.CharField(required=True, max_length=200)

    class Meta:
        model = MyUser
        fields = ('f_name', 'l_name', 'email', 'contact_no', 'clg_name', 'year', 'password1', 'password2')


class SingleEventRegistration(serializers.Serializer):
    axis_id = serializers.CharField(max_length=20)
    event_id = serializers.IntegerField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class TeamEventRegistration(serializers.Serializer):
    type = serializers.CharField(max_length=10)
    team_name = serializers.CharField(max_length=100)
    team_password = serializers.CharField(max_length=100)
    axis_id = serializers.CharField(max_length=20)
    event_id = serializers.IntegerField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class WorkshopRegistrationSerializer(serializers.Serializer):
    axis_id = serializers.CharField(max_length=20)
    workshop_id = serializers.IntegerField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass
