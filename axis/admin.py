import csv
from .forms import EmailForm
from django.core.mail import send_mail, send_mass_mail, EmailMultiAlternatives
from Axis17.settings import EMAIL_HOST_USER
from django.template.response import TemplateResponse
from django.http import HttpResponse, HttpResponseRedirect
from django.db import models
from django.forms import TextInput, Textarea
from django import forms
from django.contrib import admin
from django.conf.urls import url
from django.core.urlresolvers import reverse
from django.utils.html import format_html
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import MyUser, Event, Team, EventCategory, Request, CurrentUpdates, SingleParticipantEventRegistration, \
    TimeLine, GuestLectures, GuestLectureQuestions, Workshop, SocialInitiative, Exhibition, Talks, Schedule, \
    Inauguration, Summit, InaugurationQuestions, TechieNite


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput)

    class Meta:
        model = MyUser
        fields = ('f_name', 'l_name', 'email', 'contact_no', 'clg_name', 'year')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = MyUser
        fields = ('f_name', 'l_name', 'email', 'contact_no', 'clg_name', 'is_active', 'is_admin', 'password')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class EditProfileForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    # password = ReadOnlyPasswordHashField()

    class Meta:
        model = MyUser
        fields = ('f_name',)


class LoginForm(forms.ModelForm):
    class Meta:
        model = MyUser
        fields = ('email', 'password')


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('id', 'f_name', 'email', 'contact_no', 'clg_name', 'is_admin')
    list_filter = ('is_admin', 'is_active')

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': (('f_name', 'l_name'), ('email', 'contact_no'), ('clg_name', 'year'), 'axis_id', 'vnit_id',)}),
        ('Permissions', {'fields': ('is_admin', 'is_active',)}),
    )
    readonly_fields = ('id', 'axis_id',)
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'clg_name', 'password1', 'password2')}
        ),
    )
    search_fields = ('f_name', 'clg_name', 'email', 'axis_id')
    ordering = ('date_created',)
    filter_horizontal = ('workshop', )

    formfield_overrides = {
        models.CharField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
        models.EmailField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
    }


class GuestLectureQuestionsAdmin(admin.ModelAdmin):
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('text', 'user', 'guest_lecture')
    list_filter = ('guest_lecture',)

    # fieldsets = (
    #     (None, {'fields': ('text', 'user', 'guest_lecture')}),
    # )

    search_fields = ('user__f_name', 'user__f_name', 'user__email', 'text',)
    ordering = ('id',)
    filter_horizontal = ()

    formfield_overrides = {
        models.CharField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
    }


class GuestLectureAdmin(admin.ModelAdmin):
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('speaker_name', 'mail_actions', 'user_list_download_actions',)

    filter_horizontal = ('user',)

    fieldsets = (
        ('About Lecture', {'fields': ('speaker_name', 'about_speaker', 'about_lecture')}),
        ('Place and time', {'fields': ('venue', 'date', 'time',)}),
        (None, {'fields': ('speaker_image', 'user', 'isvalid'), }),
    )

    readonly_fields = ('id',)

    search_fields = ('speaker_name',)
    ordering = ('id',)

    formfield_overrides = {
        models.CharField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
    }

    def get_user_list(self, request, guest_lecture_id, registered):
        guest_lecture = self.get_object(request, guest_lecture_id)
        users = guest_lecture.user.all().values_list(
            'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
        )

        if not registered:
            users = MyUser.objects.all().difference(users).values_list(
                'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
            )

        column_name_list = ['First Name', 'Last Name', 'Email', 'AXIS ID', 'Contact No.', 'YOS', 'VNIT ID', 'College Name']

        return users, column_name_list, str(guest_lecture.speaker_name)

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<guest_lecture_id>.+)/send_mail_to_guest_lecture_registered_users/$',
                self.admin_site.admin_view(self.send_mail_to_guest_lecture_registered_users),
                name='send_mail_to_guest_lecture_registered_users',
            ),
            url(
                r'^(?P<guest_lecture_id>.+)/send_mail_to_guest_lecture_non_registered_users/$',
                self.admin_site.admin_view(self.send_mail_to_guest_lecture_non_registered_users),
                name='send_mail_to_guest_lecture_non_registered_users',
            ),
            url(
                r'^(?P<guest_lecture_id>.+)/download_guest_lecture_registered_users_list/$',
                self.admin_site.admin_view(self.download_guest_lecture_registered_users_list),
                name='download_guest_lecture_registered_users_list',
            ),
            url(
                r'^(?P<guest_lecture_id>.+)/download_guest_lecture_non_registered_users_list/$',
                self.admin_site.admin_view(self.download_guest_lecture_non_registered_users_list),
                name='download_guest_lecture_non_registered_users_list',
            ),
        ]
        return custom_urls + urls

    # Send mail actions
    def mail_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered Users</a>&nbsp;'
            '<a class="button" href="{}">Non Registered Users</a>',
            reverse('admin:send_mail_to_guest_lecture_registered_users', args=[obj.pk]),
            reverse('admin:send_mail_to_guest_lecture_non_registered_users', args=[obj.pk]),
        )

    mail_actions.short_description = 'Send E-Mail To'
    mail_actions.allow_tags = True

    def send_mail_to_guest_lecture_registered_users(self, request, guest_lecture_id, *args, **kwargs):
        users = ()
        if request.method == 'POST':
            users, column_name_list, workshop_name = self.get_user_list(request, guest_lecture_id, True)
        return send_email_form(request, self.admin_site.each_context(request),
                               self.model._meta, 'admin:axis_guestlectures_changelist', users)

    def send_mail_to_guest_lecture_non_registered_users(self, request, guest_lecture_id, *args, **kwargs):
        users = ()
        if request.method == 'POST':
            users, column_name_list, workshop_name = self.get_user_list(request, guest_lecture_id, False)
        return send_email_form(request, self.admin_site.each_context(request),
                               self.model._meta, 'admin:axis_guestlectures_changelist', users)

    # User list download actions
    def user_list_download_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered</a>&nbsp;'
            '<a class="button" href="{}">Non Registered</a>',
            reverse('admin:download_guest_lecture_registered_users_list', args=[obj.pk]),
            reverse('admin:download_guest_lecture_non_registered_users_list', args=[obj.pk]),
        )

    user_list_download_actions.short_description = 'Download User List'
    user_list_download_actions.allow_tags = True

    def download_guest_lecture_registered_users_list(self, request, guest_lecture_id, *args, **kwargs):
        users, column_name_list, guest_lecture_name = self.get_user_list(request, guest_lecture_id, True)
        return download_csv(users, column_name_list, guest_lecture_name + '-registered-users.csv')

    def download_guest_lecture_non_registered_users_list(self, request, guest_lecture_id, *args, **kwargs):
        users, column_name_list, guest_lecture_name = self.get_user_list(request, guest_lecture_id, False)
        return download_csv(users, column_name_list, guest_lecture_name + '-non-registered-users.csv')


class EventAdmin(admin.ModelAdmin):
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('name', 'mail_actions', 'user_list_download_actions',)
    list_filter = ('category',)

    readonly_fields = ('id',)

    search_fields = ('name',)
    ordering = ('name',)
    filter_horizontal = ()

    formfield_overrides = {
        models.CharField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
    }

    def get_user_list(self, request, event_id, registered):
        event = self.get_object(request, event_id)
        # Event.objects.all()[0].team_set.all()[0].users.all()
        # users = event.registered_users.all().values_list(
        #     'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
        # )
        #
        users = MyUser.objects.none().values_list(
                'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
            )

        for team in event.team_set.all():
            users = users.union(team.users.all().values_list(
                'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
            ))

        for spo in event.singleparticipanteventregistration_set.all():
            users = users.union(MyUser.objects.filter(singleparticipanteventregistration=spo).values_list(
                'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
            ))

        # users = MyUser.objects.all()[]
        #     filter(event_id=event_id).values_list(
        #     'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
        # )

        if not registered:
            users = MyUser.objects.all().difference(users).values_list(
                'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
            )

        column_name_list = ['First Name', 'Last Name', 'Email', 'AXIS ID', 'Contact No.', 'YOS', 'VNIT ID', 'College Name']

        return users, column_name_list, str(event.name)

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<event_id>.+)/send_mail_event_to_registered_users/$',
                self.admin_site.admin_view(self.send_mail_to_event_registered_users),
                name='send_mail_to_event_registered_users',
            ),
            url(
                r'^(?P<event_id>.+)/send_mail_event_to_non_registered_users/$',
                self.admin_site.admin_view(self.send_mail_to_event_non_registered_users),
                name='send_mail_to_event_non_registered_users',
            ),
            url(
                r'^(?P<event_id>.+)/download_event_registered_users_list/$',
                self.admin_site.admin_view(self.download_event_registered_users_list),
                name='download_event_registered_users_list',
            ),
            url(
                r'^(?P<event_id>.+)/download_event_non_registered_users_list/$',
                self.admin_site.admin_view(self.download_event_non_registered_users_list),
                name='download_event_non_registered_users_list',
            ),
        ]
        return custom_urls + urls

    # Send mail actions
    def mail_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered Users</a>&nbsp;'
            '<a class="button" href="{}">Non Registered Users</a>',
            reverse('admin:send_mail_to_event_registered_users', args=[obj.pk]),
            reverse('admin:send_mail_to_event_non_registered_users', args=[obj.pk]),
        )

    mail_actions.short_description = 'Send E-Mail To'
    mail_actions.allow_tags = True

    def send_mail_to_event_registered_users(self, request, event_id, *args, **kwargs):
        users = ()
        if request.method == 'POST':
            users, column_name_list, event_name = self.get_user_list(request, event_id, True)
        return send_email_form(request, self.admin_site.each_context(request),
                               self.model._meta, 'admin:axis_event_changelist', users)

    def send_mail_to_event_non_registered_users(self, request, event_id, *args, **kwargs):
        users = ()
        if request.method == 'POST':
            users, column_name_list, event_name = self.get_user_list(request, event_id, False)
        return send_email_form(request, self.admin_site.each_context(request),
                               self.model._meta, 'admin:axis_event_changelist', users)

    # User list download actions
    def user_list_download_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered</a>&nbsp;'
            '<a class="button" href="{}">Non Registered</a>',
            reverse('admin:download_event_registered_users_list', args=[obj.pk]),
            reverse('admin:download_event_non_registered_users_list', args=[obj.pk]),
        )

    user_list_download_actions.short_description = 'Download User List'
    user_list_download_actions.allow_tags = True

    def download_event_registered_users_list(self, request, event_id, *args, **kwargs):
        users, column_name_list, event_name = self.get_user_list(request, event_id, True)
        return download_csv(users, column_name_list, event_name + '-registered-users.csv')

    def download_event_non_registered_users_list(self, request, event_id, *args, **kwargs):
        users, column_name_list, event_name = self.get_user_list(request, event_id, False)
        return download_csv(users, column_name_list, event_name + '-non-registered-users.csv')


class TalkAdmin(admin.ModelAdmin):
    list_display = ('name', 'mail_actions', 'user_list_download_actions',)

    filter_horizontal = ('user',)

    fieldsets = (
        ('About Lecture', {'fields': ('name', 'description',)}),
        ('Place and time', {'fields': ('venue', 'date', 'time',)}),
        (None, {'fields': ('image', 'user', 'link',)}),
    )

    readonly_fields = ('id',)

    search_fields = ('name',)
    ordering = ('id',)

    formfield_overrides = {
        models.CharField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
    }

    def get_user_list(self, request, talk_id, registered):
        talk = self.get_object(request, talk_id)
        users = talk.user.all().values_list(
            'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
        )

        if not registered:
            users = MyUser.objects.all().difference(users).values_list(
                'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
            )

        column_name_list = ['First Name', 'Last Name', 'Email', 'AXIS ID', 'Contact No.', 'YOS', 'VNIT ID', 'College Name']

        return users, column_name_list, str(talk.name)

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<talk_id>.+)/send_mail_to_talk_registered_users/$',
                self.admin_site.admin_view(self.send_mail_to_talk_registered_users),
                name='send_mail_to_talk_registered_users',
            ),
            url(
                r'^(?P<talk_id>.+)/send_mail_to_talk_non_registered_users/$',
                self.admin_site.admin_view(self.send_mail_to_talk_non_registered_users),
                name='send_mail_to_talk_non_registered_users',
            ),
            url(
                r'^(?P<talk_id>.+)/download_talk_registered_users_list/$',
                self.admin_site.admin_view(self.download_talk_registered_users_list),
                name='download_talk_registered_users_list',
            ),
            url(
                r'^(?P<talk_id>.+)/download_talk_non_registered_users_list/$',
                self.admin_site.admin_view(self.download_talk_non_registered_users_list),
                name='download_talk_non_registered_users_list',
            ),
        ]
        return custom_urls + urls

    # Send mail actions
    def mail_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered Users</a>&nbsp;'
            '<a class="button" href="{}">Non Registered Users</a>',
            reverse('admin:send_mail_to_talk_registered_users', args=[obj.pk]),
            reverse('admin:send_mail_to_talk_non_registered_users', args=[obj.pk]),
        )

    mail_actions.short_description = 'Send E-Mail To'
    mail_actions.allow_tags = True

    def send_mail_to_talk_registered_users(self, request, talk_id, *args, **kwargs):
        users = ()
        if request.method == 'POST':
            users, column_name_list, talk_name = self.get_user_list(request, talk_id, True)
        return send_email_form(request, self.admin_site.each_context(request),
                               self.model._meta, 'admin:axis_talks_changelist', users)

    def send_mail_to_talk_non_registered_users(self, request, talk_id, *args, **kwargs):
        users = ()
        if request.method == 'POST':
            users, column_name_list, talk_name = self.get_user_list(request, talk_id, False)
        return send_email_form(request, self.admin_site.each_context(request),
                               self.model._meta, 'admin:axis_talks_changelist', users)

    # User list download actions
    def user_list_download_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered</a>&nbsp;'
            '<a class="button" href="{}">Non Registered</a>',
            reverse('admin:download_talk_registered_users_list', args=[obj.pk]),
            reverse('admin:download_talk_non_registered_users_list', args=[obj.pk]),
        )

    user_list_download_actions.short_description = 'Download User List'
    user_list_download_actions.allow_tags = True

    def download_talk_registered_users_list(self, request, talk_id, *args, **kwargs):
        users, column_name_list, guest_lecture_name = self.get_user_list(request, talk_id, True)
        return download_csv(users, column_name_list, guest_lecture_name + '-registered-users.csv')

    def download_talk_non_registered_users_list(self, request, talk_id, *args, **kwargs):
        users, column_name_list, guest_lecture_name = self.get_user_list(request, talk_id, False)
        return download_csv(users, column_name_list, guest_lecture_name + '-non-registered-users.csv')


class TeamAdmin(admin.ModelAdmin):
    list_display = ('team_name', 'event',)# 'user_list_download_actions',)
    list_filter = ('event',)
    filter_horizontal = ('users',)

    # fieldsets = (
    #     ('About Lecture', {'fields': ('name', 'description',)}),
    #     ('Place and time', {'fields': ('venue', 'date', 'time',)}),
    #     (None, {'fields': ('image', 'user', 'link',)}),
    # )

    readonly_fields = ('id',)

    search_fields = ('name',)
    ordering = ('id',)

    formfield_overrides = {
        models.CharField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
    }

    # def

    # def send_mail_to_teams(self, request, queryset):
    #     if request.method == "POST":


# @admin.register(Workshop)
class WorkshopAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'registrations_closed', 'payment_link', 'mail_actions', 'user_list_download_actions',)
    filter_horizontal = ('registered_users',)

    formfield_overrides = {
        models.CharField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
    }

    # def formfield_for_manytomany(self, db_field, request, **kwargs):
    #     if db_field.name == "registered_users":
    #         if __name__ == '__main__':
    #             kwargs["queryset"] = db_field.remote_field
    #     return super().formfield_for_manytomany(db_field, request, **kwargs)

    def get_user_list(self, request, workshop_id, registered):
        workshop = self.get_object(request, workshop_id)
        users = workshop.registered_users.all().values_list(
            'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
        )

        if not registered:
            users = MyUser.objects.all().difference(users).values_list(
                'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
            )

        column_name_list = ['First Name', 'Last Name', 'Email', 'AXIS ID', 'Contact No.', 'YOS', 'VNIT ID', 'College Name']

        return users, column_name_list, str(workshop.name)

    def get_urls(self):
        urls = super(WorkshopAdmin, self).get_urls()
        custom_urls = [
            url(
                r'^(?P<workshop_id>.+)/send_mail_to_workshop_registered_users/$',
                self.admin_site.admin_view(self.send_mail_to_workshop_registered_users),
                name='send_mail_to_workshop_registered_users',
            ),
            url(
                r'^(?P<workshop_id>.+)/send_mail_to_workshop_non_registered_users/$',
                self.admin_site.admin_view(self.send_mail_to_workshop_non_registered_users),
                name='send_mail_to_workshop_non_registered_users',
            ),
            url(
                r'^(?P<workshop_id>.+)/download_workshop_registered_users_list/$',
                self.admin_site.admin_view(self.download_workshop_registered_users_list),
                name='download_workshop_registered_users_list',
            ),
            url(
                r'^(?P<workshop_id>.+)/download_workshop_non_registered_users_list/$',
                self.admin_site.admin_view(self.download_workshop_non_registered_users_list),
                name='download_workshop_non_registered_users_list',
            ),
        ]
        return custom_urls + urls

    # Send mail actions
    def mail_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered Users</a>&nbsp;'
            '<a class="button" href="{}">Non Registered Users</a>',
            reverse('admin:send_mail_to_workshop_registered_users', args=[obj.pk]),
            reverse('admin:send_mail_to_workshop_non_registered_users', args=[obj.pk]),
        )

    mail_actions.short_description = 'Send E-Mail To'
    mail_actions.allow_tags = True

    def send_mail_to_workshop_registered_users(self, request, workshop_id, *args, **kwargs):
        users = ()
        if request.method == 'POST':
            users, column_name_list, workshop_name = self.get_user_list(request, workshop_id, True)
        return send_email_form(request, self.admin_site.each_context(request),
                               self.model._meta, 'admin:axis_workshop_changelist', users)

    def send_mail_to_workshop_non_registered_users(self, request, workshop_id, *args, **kwargs):
        users = ()
        if request.method == 'POST':
            users, column_name_list, workshop_name = self.get_user_list(request, workshop_id, False)
        return send_email_form(request, self.admin_site.each_context(request),
                               self.model._meta, 'admin:axis_workshop_changelist', users)

    # User list download actions
    def user_list_download_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered</a>&nbsp;'
            '<a class="button" href="{}">Non Registered</a>',
            reverse('admin:download_workshop_registered_users_list', args=[obj.pk]),
            reverse('admin:download_workshop_non_registered_users_list', args=[obj.pk]),
        )

    user_list_download_actions.short_description = 'Download User List'
    user_list_download_actions.allow_tags = True

    def download_workshop_registered_users_list(self, request, workshop_id, *args, **kwargs):
        users, column_name_list, workshop_name = self.get_user_list(request, workshop_id, True)
        return download_csv(users, column_name_list, workshop_name + '-registered-users.csv')

    def download_workshop_non_registered_users_list(self, request, workshop_id, *args, **kwargs):
        users, column_name_list, workshop_name = self.get_user_list(request, workshop_id, False)
        return download_csv(users, column_name_list, workshop_name + '-non-registered-users.csv')


def send_email_form(request, each_context, _meta, redirect_to, users):
    if request.method != 'POST':
        form = EmailForm()
    else:
        form = EmailForm(request.POST)

        if form.is_valid():
            user_list = []

            for u in users:
                user_list.append(u[2])

            # send_mass_mail(((form.cleaned_data['subject'], form.cleaned_data['body'], EMAIL_HOST_USER, user_list),),
            #                fail_silently=False)

            msg = EmailMultiAlternatives(
                form.cleaned_data['subject'],
                form.cleaned_data['body'],
                EMAIL_HOST_USER,
                bcc=user_list
            )

            msg.send()

            return HttpResponseRedirect(reverse(redirect_to))

    context = each_context
    context['opts'] = _meta
    context['form'] = form
    context['title'] = "Send E-mail"

    return TemplateResponse(
        request,
        'axis/send_email_form.html',
        context,
    )


def download_csv(query_set, column_name_list, file_name):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + file_name + '"'

    writer = csv.writer(response)
    writer.writerow(column_name_list)

    for q in query_set:
        writer.writerow(q)

    return response


class ExhibitionAdmin(admin.ModelAdmin):
    # list_display = ('team_name', 'event',)# 'user_list_download_actions',)
    # list_filter = ('event',)
    # filter_horizontal = ('users',)

    fieldsets = (
        ('About Exhibition', {'fields': ('name', 'description',)}),
        ('Place and time', {'fields': ('venue', 'date', 'time',)}),
        (None, {'fields': ('image', 'background_image',)}),
    )

    # readonly_fields = ('id',)

    search_fields = ('name',)
    ordering = ('id',)

    # formfield_overrides = {
    #     models.CharField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})},
    # }

    # def

    # def send_mail_to_teams(self, request, queryset):
    #     if request.method == "POST":


class InaugurationAdmin(admin.ModelAdmin):
    list_display = ('guest_name', 'user_list_download_actions')

    def get_user_list(self, request, inauguration_id, registered):
        inauguration = self.get_object(request, inauguration_id)
        users = inauguration.user.all().values_list(
            'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
        )

        if not registered:
            users = MyUser.objects.all().difference(users).values_list(
                'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
            )

        column_name_list = ['First Name', 'Last Name', 'Email', 'AXIS ID', 'Contact No.', 'YOS', 'VNIT ID', 'College Name']

        return users, column_name_list, str(inauguration.guest_name)

    def get_urls(self):
        urls = super(InaugurationAdmin, self).get_urls()
        custom_urls = [
            url(
                r'^(?P<inauguration_id>.+)/download_inauguration_registered_users_list/$',
                self.admin_site.admin_view(self.download_inauguration_registered_users_list),
                name='download_inauguration_registered_users_list',
            ),
            url(
                r'^(?P<inauguration_id>.+)/download_inauguration_non_registered_users_list/$',
                self.admin_site.admin_view(self.download_inauguration_non_registered_users_list),
                name='download_inauguration_non_registered_users_list',
            ),
        ]
        return custom_urls + urls

    # User list download actions
    def user_list_download_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered</a>&nbsp;'
            '<a class="button" href="{}">Non Registered</a>',
            reverse('admin:download_inauguration_registered_users_list', args=[obj.pk]),
            reverse('admin:download_inauguration_non_registered_users_list', args=[obj.pk]),
        )

    user_list_download_actions.short_description = 'Download User List'
    user_list_download_actions.allow_tags = True

    def download_inauguration_registered_users_list(self, request, inauguration_id, *args, **kwargs):
        users, column_name_list, inauguration_name = self.get_user_list(request, inauguration_id, True)
        return download_csv(users, column_name_list, inauguration_name + '-registered-users.csv')

    def download_inauguration_non_registered_users_list(self, request, inauguration_id, *args, **kwargs):
        users, column_name_list, inauguration_name = self.get_user_list(request, inauguration_id, False)
        return download_csv(users, column_name_list, inauguration_name + '-non-registered-users.csv')


class SummitAdmin(admin.ModelAdmin):
    list_display = ('speaker_name', 'user_list_download_actions')

    def get_user_list(self, request, summit_id, registered):
        summit = self.get_object(request, summit_id)
        users = summit.user.all().values_list(
            'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
        )

        if not registered:
            users = MyUser.objects.all().difference(users).values_list(
                'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
            )

        column_name_list = ['First Name', 'Last Name', 'Email', 'AXIS ID', 'Contact No.', 'YOS', 'VNIT ID', 'College Name']

        return users, column_name_list, str(summit.speaker_name)

    def get_urls(self):
        urls = super(SummitAdmin, self).get_urls()
        custom_urls = [
            url(
                r'^(?P<summit_id>.+)/download_summit_registered_users_list/$',
                self.admin_site.admin_view(self.download_summit_registered_users_list),
                name='download_summit_registered_users_list',
            ),
            url(
                r'^(?P<inauguration_id>.+)/download_summit_non_registered_users_list/$',
                self.admin_site.admin_view(self.download_summit_non_registered_users_list),
                name='download_summit_non_registered_users_list',
            ),
        ]
        return custom_urls + urls

    # User list download actions
    def user_list_download_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered</a>&nbsp;'
            '<a class="button" href="{}">Non Registered</a>',
            reverse('admin:download_summit_registered_users_list', args=[obj.pk]),
            reverse('admin:download_summit_non_registered_users_list', args=[obj.pk]),
        )

    user_list_download_actions.short_description = 'Download User List'
    user_list_download_actions.allow_tags = True

    def download_summit_registered_users_list(self, request, summit_id, *args, **kwargs):
        users, column_name_list, summit_name = self.get_user_list(request, summit_id, True)
        return download_csv(users, column_name_list, summit_name + '-registered-users.csv')

    def download_summit_non_registered_users_list(self, request, summit_id, *args, **kwargs):
        users, column_name_list, summit_name = self.get_user_list(request, summit_id, False)
        return download_csv(users, column_name_list, summit_name + '-non-registered-users.csv')


class TechieNiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'user_list_download_actions')

    def get_user_list(self, request, techiNite_id, registered):
        techiNite = self.get_object(request, techiNite_id)
        users = techiNite.user.all().values_list(
            'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
        )

        if not registered:
            users = MyUser.objects.all().difference(users).values_list(
                'f_name', 'l_name', 'email', 'axis_id', 'contact_no', 'year', 'vnit_id', 'clg_name'
            )

        column_name_list = ['First Name', 'Last Name', 'Email', 'AXIS ID', 'Contact No.', 'YOS', 'VNIT ID', 'College Name']

        return users, column_name_list, str(techiNite.name)

    def get_urls(self):
        urls = super(TechieNiteAdmin, self).get_urls()
        custom_urls = [
            url(
                r'^(?P<techiNite_id>.+)/download_techiNite_registered_users_list/$',
                self.admin_site.admin_view(self.download_techiNite_registered_users_list),
                name='download_techiNite_registered_users_list',
            ),
            url(
                r'^(?P<techiNite_id>.+)/download_techiNite_non_registered_users_list/$',
                self.admin_site.admin_view(self.download_techiNite_non_registered_users_list),
                name='download_techiNite_non_registered_users_list',
            ),
        ]
        return custom_urls + urls

    # User list download actions
    def user_list_download_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Registered</a>&nbsp;'
            '<a class="button" href="{}">Non Registered</a>',
            reverse('admin:download_techiNite_registered_users_list', args=[obj.pk]),
            reverse('admin:download_techiNite_non_registered_users_list', args=[obj.pk]),
        )

    user_list_download_actions.short_description = 'Download User List'
    user_list_download_actions.allow_tags = True

    def download_techiNite_registered_users_list(self, request, techiNite_id, *args, **kwargs):
        users, column_name_list, techiNite_name = self.get_user_list(request, techiNite_id, True)
        return download_csv(users, column_name_list, techiNite_name + '-registered-users.csv')

    def download_techiNite_non_registered_users_list(self, request, techiNite_id, *args, **kwargs):
        users, column_name_list, techiNite_name = self.get_user_list(request, techiNite_id, False)
        return download_csv(users, column_name_list, techiNite_name + '-non-registered-users.csv')


# Now register the new UserAdmin...


admin.site.register(Event, EventAdmin)
admin.site.register(EventCategory)
admin.site.register(MyUser, UserAdmin)
admin.site.unregister(Group)
admin.site.register(Team, TeamAdmin)
admin.site.register(Request)
admin.site.register(TimeLine)
admin.site.register(GuestLectures, GuestLectureAdmin)
admin.site.register(GuestLectureQuestions, GuestLectureQuestionsAdmin)
admin.site.register(Workshop, WorkshopAdmin)
admin.site.register(Exhibition, ExhibitionAdmin)
admin.site.register(SingleParticipantEventRegistration)
admin.site.register(CurrentUpdates)
admin.site.register(SocialInitiative)
admin.site.register(Talks, TalkAdmin)
admin.site.register(Schedule)
admin.site.register(Inauguration, InaugurationAdmin)
admin.site.register(Summit, SummitAdmin)
admin.site.register(TechieNite, TechieNiteAdmin)
admin.site.register(InaugurationQuestions)
