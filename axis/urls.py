from django.conf.urls import url, include
from django.contrib import admin
# from django.contrib.sitemaps.views import sitemap
# from axis.sitemaps import *

from . import views

app_name = 'axis'

# Dictionary containing your sitemap classes
# sitemaps = {
#    'static': StaticSitemap,
# }

urlpatterns = [

    url(r'^$', views.home, name='home'),
    url(r'^team/$', views.team, name='team'),
    url(r'^tns/$', views.tns, name='tns'),
    url(r'^tshirt/$', views.tshirt, name='tshirt'),
    url(r'^inauguration/$', views.inauguration, name='inauguration'),
    url(r'^techie-nite/$', views.techie_nite, name='techie_nite'),
    # url(r'^tyou/$', views.tyou, name='tyou'),
    # url(r'^bakerstreet/$', views.bakerstreet, name='bakerstreet'),
    # url(r'^bakerstreet_rules/$', views.bakerstreet_rules, name='bakerstreet_rules'),
    url(r'^logout/$', views.log_out, name='logout'),
    url(r'^about/$', views.about, name='about'),
    url(r'^event_categories/$', views.event_categories, name='event_categories'),
    # url(r'^event_categories/(?P<size>[0-9]+)/$', views.set_size_redirect, name='set_size_redirect'),
    url(r'^event_categories/$', views.event_categories, name='event_categories'),
    # url(r'^event_categories/(?P<slug>[\w-]+)/$', views.events, name='events'),
    url(r'^register_for_event/(?P<event_id>[0-9]+)/$', views.register_for_event, name='register_for_event'),
    url(r'^create_team/(?P<event_id>[0-9]+)/$', views.create_team, name='create_team'),
    url(r'^workshops/$', views.workshops, name='workshops'),
    url(r'^validate_email/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', views.validate_email, name="validate_email"),
    # url(r'^googletokensignin', views.google_signin, name="googletokensignin"),
    url(r'^updates/$', views.updates, name='updates'),
    url(r'^sponsors/$', views.sponsors, name='sponsors'),
    url(r'^attractions/$', views.attractions, name='attractions'),
    url(r'^guestlectures/$', views.guestlectures, name='guestlectures'),
    url(r'^talks/$', views.talks, name='talks'),
    url(r'^social_initiatives/$', views.initiatives, name='social_initiatives'),
    url(r'^exhibition/$', views.exhibitions, name='exhibition'),
    url(r'^googleb1165711f14fb6e5.html/$', views.google_site_verification, name='google_site_verification'),
    url(r'^sitemap/$', views.sitemap, name='sitemap'),
    url(r'^api/', include('axis.api_urls', namespace="api_urls")),
    # url(r'^api/events$', views.event_list, name='api_event_list'),
    # url(r'^api/workshops$', views.workshops_list, name='api_workshops_list'),
    # url(r'^api/guest_lectures$', views.guest_lectures_list, name='api_guest_lectures_list'),
    # url(r'^api/social_initiatives$', views.social_initiatives_list, name='api_social_initiatives_list'),
    # url(r'^api/exhibitions$', views.exhibitions_list, name='api_exhibitions_list'),
    # url(r'^api/talks$', views.talks_list, name='api_talks_list'),
    # url(r'^api/timeline$', views.timeline_list, name='api_timeline_list'),
    # url(r'^api/current_updates$', views.current_updates_list, name='api_current_updates_list'),
    # url(r'^api/event_categories$', views.event_category_list, name='api_event_category_list'),
    # url(r'^api/login$', views.api_login, name='api_login'),
    # url(r'^events/(?P<cat_slug>[\w-]+)/(?P<event_slug>[\w-]+)/$', views.event, name='event'),
    # url(r'^requests/$', views.requests, name='requests'),
    # url(r'^requests/accept/(?P<team_id>[0-9]+)/$', views.accept_request, name='accept_request'),
    # url(r'^requests/reject/(?P<req_id>[0-9]+)/$', views.reject_request, name='reject_request'),
    # url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
    #     name='django.contrib.sitemaps.views.sitemap'),
]


