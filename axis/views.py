from django.contrib.auth.tokens import default_token_generator
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.decorators.csrf import csrf_exempt
from rest_framework.utils import json

from axis.admin import UserCreationForm, UserChangeForm
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.http import HttpResponse
from .models import MyUser, Event, EventCategory, Team, Request, CurrentUpdates, SingleParticipantEventRegistration, \
    TimeLine, GuestLectures, GuestLectureQuestions, Workshop, SocialInitiative, Exhibition, Talks, Schedule, \
    Inauguration, Summit, InaugurationQuestions, TechieNite

from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from Axis17.settings import EMAIL_HOST_USER
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .serializers import EventSerializer, EventCategorySerializer, MyUserLoginSerializer, \
    WorkshopSerializer, GuestLecturesSerializer, SocialInitiativeSerializer, ExhibitionSerializer, TalksSerializer, \
    TimeLineSerializer, CurrentUpdatesSerializer, UserCreationSerializer, \
    SingleEventRegistration, TeamEventRegistration, WorkshopRegistrationSerializer, ScheduleSerializer, \
    InaugurationSerializer, SummitSerializer, TechieNiteSerializer


def about(request):
    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis/about.html')
    else:
        return render(request, 'axis/about.html', {'user_registrations': UserRegistration(request.user)})


def team(request):
    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:team')
    else:
        return render(request, 'axis/team.html',
                      {'user_registrations': UserRegistration(request.user)})

def tns(request):
    Summits = Summit.objects.all().order_by('id')
    if request.method == 'POST' and ('Login' in request.POST or 'Signup' in request.POST):
        return login_signup(request, 'axis:tns', {'Summits': Summits})

    elif request.method == 'POST' and ('register' in request.POST):
        for summit in Summits:
            summit.user.add(request.user)
            summit.save()
        messages.success(request, "Successfully registered for TNS!")
        return redirect('axis:tns')
    # elif request.method == 'POST' and ('submit_question' in request.POST) and (request.POST['question'] != ''):
    #     text = request.POST['question']
    #     inaug_question = InaugurationQuestions()
    #     inaug_question.user = request.user
    #     inaug_question.text = text
    #     inaug_question.save()
    #     messages.info(request, "Question Submitted!")
    #     return redirect('axis:inauguration')
    else:
        is_registered = False
        is_closed = False
        if request.user.is_authenticated:
            user = request.user
            for summit in Summits:
                if summit.registrations_closed:
                    is_closed = True
                if summit in user.summit_set.all():
                    is_registered = True

        return render(request, 'axis/tns.html',
                      {'Summits': Summits,
                       'is_registered': is_registered, 'is_closed': is_closed})


# pata nahi kisne kiya ye
#
#
# def login_signups(request, page, context={}):
#     request.session['message'] = ""
#     request.session['message_type'] = ""
#     if 'Login' in request.POST:
#         email = request.POST['email']
#         password = request.POST['password']
#         flag = True
#         # u = get_object_or_404(MyUser, email=email)
#         try:
#             u = MyUser.objects.get(email=email)
#         except ObjectDoesNotExist:
#             request.session['message'] = "Invalid Credentials"
#             request.session['message_type'] = "error"
#             __context = {"message_type": request.session['message_type'],
#                          "message": request.session['message'],
#                          }
#             __context.update(context)
#             return render(request, page, __context)
#
#         '''
#         if u is None:
#             request.session['message'] = "Invalid Credentials"
#             request.session['message_type'] = "error"
#             __context = {"message_type": request.session['message_type'],
#                                                       "message": request.session['message'],
#                                                     }
#             __context.update(context)
#             return render(request, page, __context)
#         '''
#         if not u.is_active:
#             print(u.f_name)
#             flag = False
#             u.is_active = True
#             u.save()
#
#         user = authenticate(email=email, password=password)
#         # print(user.l_name)
#         if not flag:
#             # u = MyUser.objects.get(email=email)
#             print(user.axis_id)
#             user.is_active = False
#             user.save()
#             flag = True
#         # print(user.f_name)
#         if user is not None:
#             if user.is_active:
#                 # print("active")
#                 login(request, user)
#                 request.session['message'] = "You've been successfully logged in!"
#                 request.session['message_type'] = "success"
#                 __context = {
#                     "message_type": request.session['message_type'],
#                     "message": request.session['message'],
#                     "remove_hash": "remove_hash",
#                     'increase_delay': "increase_delay"
#                 }
#                 __context.update(context)
#                 return render(request, page, __context)
#             else:
#                 # print("not active!!!!!!")
#                 request.session['message'] = "Please validate your account first!"
#                 request.session['message_type'] = "error"
#                 __context = {
#                     "message_type": request.session['message_type'],
#                     "message": request.session['message'],
#                     "remove_hash": "remove_hash",
#                     "increase_delay": "increase_delay",
#                 }
#                 __context.update(context)
#                 return render(request, page, __context)
#
#         else:
#             # print("hahaha")
#             request.session['message'] = "Invalid Credentials"
#             request.session['message_type'] = "error"
#             __context = {
#                 "message_type": request.session['message_type'],
#                 "message": request.session['message'],
#                 "increase_delay": "increase_delay",
#             }
#             __context.update(context)
#             return render(request, page, __context)
#
#     elif "Signup" in request.POST:
#         form = UserCreationForm(request.POST or None)
#         if form.is_valid():
#             user = form.save(commit=False)
#             email = form.cleaned_data['email']
#             password = form.cleaned_data['password1']
#             user.set_password(password)
#             user.date_created = datetime.now()
#             user.is_active = False
#             user.is_admin = False
#             user.save()
#             user.axis_id = 'AXIS18' + (str(user.pk + 4235).zfill(5))
#             user.save()
#             # print(user.axis_id)
#             link = request.META['HTTP_HOST']
#             send_validation_email(user, link, password)
#             request.session['message'] = "Successfully Registered! Please check your mail to validate your account!!"
#             request.session['message_type'] = "success"
#             __context = {
#                 "message_type": request.session['message_type'],
#                 "message": request.session['message'],
#                 "remove_hash": "remove_hash",
#                 "increase_delay": "increase_delay",
#             }
#             __context.update(context)
#             return render(request, page, __context)
#
#         else:
#             request.session['message'] = "Email Id already exists! Please try signing up using different email" \
#                                          " address."
#             request.session['message_type'] = "error"
#             __context = {
#                 "message_type": request.session['message_type'],
#                 "message": request.session['message'],
#                 "increase_delay": "increase_delay",
#             }
#             __context.update(context)
#             return render(request, page, __context)
#
#
# def homes(request):
#     request.session['message'] = ""
#     request.session['message_type'] = ""
#     if request.method == 'POST' and ('Login' in request.POST or 'Signup' in request.POST):
#         return login_signup(request, "axis/home.html")
#     else:
#         current_updates = CurrentUpdates.objects.all()
#
#         return render(request, 'axis/home.html',
#                       {
#                           'user_registrations': UserRegistration(request.user),
#                           "message_type": request.session['message_type'],
#                           "message": request.session['message'],
#                           "current_updates": current_updates
#                       })
#


def log_out(request):
    logout(request)
    return redirect("axis:home")


def event_categories(request):
    if ('password' in request.POST or 'password1' in request.POST) and ('email' in request.POST):
        return login_signup(request, 'axis:event_categories')

    if request.method == 'POST':
        print("post request")
        register_for_event(request)
        return redirect(request.path)

    event_categories = EventCategory.objects.all().order_by('name')
    events = Event.objects.all().order_by('name')

    html_page = 'axis/event_categories_desktop.html'

    return render(request, html_page,
                  {
                      'user_registrations': UserRegistration(request.user),
                      'event_categories': event_categories, 'events': events,
                      'today': datetime.now()
                  })


# return render(request, 'axis/filter.html')


def register_for_event(request):
    if request.method == 'POST':
        event_id = request.POST['eventId']
        # print(event_id)
        if 'CreateTeam' in request.POST:
            return create_team(request, event_id)
        elif 'JoinTeam' in request.POST:
            return join_team(request, event_id)
        elif 'Register' in request.POST:
            return register_user_for_single_participant_event(request, event_id)

    messages.error(request, "Invalid Request")


def create_team(request, event_id):
    __team_name = str(request.POST['teamName'])
    __team_password = str(request.POST['password'])

    try:
        # Check if team name is already taken
        if Team.objects.get(team_name=__team_name):
            messages.error(request, "Team name: " + __team_name + " already taken.")
    except ObjectDoesNotExist:
        Team().set_values(__team_name, Event.objects.get(id=event_id), request.user, __team_password)
        messages.success(request,
                         "Successfully registered and created team " + __team_name + " for " + Event.objects.get(
                             id=event_id).name)
        # event_registration_email(request.user, Event.objects.get(id=event_id).name, __team_name, __team_password)


def join_team(request, event_id):
    __team_name = request.POST['teamName']
    __team_password = request.POST['password']

    try:
        team = Team.objects.get(team_name=__team_name)

        # Check if password matches then let user join the team
        if team.check_password(__team_password) and team.event.id == int(event_id):
            team.add_user(request.user)
            # Todo: handle success response
            messages.success(request,
                             "You have successfully joined the team " + team.team_name + " for " + team.event.name)
            # event_registration_email(request.user, Event.objects.get(id=event_id).name, __team_name, __team_password)
        else:
            messages.error(request, 'Invalid Credentials to join a team for ' + Event.objects.get(id=event_id).name)
            # return HttpResponse("Invalid Credentials")

    except ObjectDoesNotExist:
        # Todo: handle failure response
        messages.error(request, 'Invalid Credentials to join a team for' + Event.objects.get(id=event_id).name)


def register_user_for_single_participant_event(request, event_id):
    # Todo: handle responses
    try:
        SingleParticipantEventRegistration.objects.get(user=request.user, event=Event.objects.get(id=event_id))
        messages.info(request, 'You are already registered for ' + Event.objects.get(id=event_id).name)
    except ObjectDoesNotExist:
        try:
            registration = SingleParticipantEventRegistration()
            registration.user = request.user
            registration.event = Event.objects.get(id=event_id)
            registration.save()
            messages.success(request, "Successfully registered for " + registration.event.name)
            # event_registration_email(request.user, Event.objects.get(id=event_id).name)
        except ObjectDoesNotExist:
            messages.error(request, "Event does not exist")


def events(request, slug):
    cat = EventCategory.objects.get(slug=slug)
    # cat = get_object_or_404(EventCategory, slug=slug)
    eventss = Event.objects.filter(category=cat.pk)

    return render(request, 'axis/events.html', {'events': eventss, 'cat_slug': slug, })


class RegisteredEventsDetails:
    def __init__(self, __event_name="", __team_members="", __team_name="---"):
        self.eventName = __event_name
        self.teamName = __team_name
        self.teamMembers = __team_members


class UserRegistration:
    def __init__(self, user):
        self.event_id_list, self.event = get_user_registered_events(user)
        self.guest_lecture = get_user_registered_guest_lectures(user)
        self.workshop = get_user_registered_workshops(user)
        # print(self.event_id_list)


def get_user_registered_workshops(user):
    if user.is_anonymous:
        return

    return user.workshop_set.values('id', 'name')


def get_user_registered_events(user):
    if user.is_anonymous:
        return None, None

    registered_for = []
    registered_events_id_list = []

    # Extracting details of single participant event details
    for spe in SingleParticipantEventRegistration.objects.filter(user=user):
        registered_for.append(RegisteredEventsDetails(spe.event.name, user.f_name + " " + user.l_name))
        registered_events_id_list.append(spe.event.id)

    # Extracting details of team event details
    for t in user.team_set.all():
        registered_events_id_list.append(t.event.id)
        s = ""
        for u in t.users.all():
            s += str(u) + ", "

        r = RegisteredEventsDetails(t.event.name, s, t.team_name)
        registered_for.append(r)

    return registered_events_id_list, registered_for


def get_user_registered_guest_lectures(user):
    if user.is_anonymous:
        return

    return user.guestlectures_set.values('id', 'speaker_name')


# MyUser.objects.all()[0].workshop_set.values('id', 'name')


def workshops(request):
    ws = Workshop.objects.all().order_by('id')
    workshop_registered_for = []

    if not request.user.is_anonymous:
        for workshop in request.user.workshop_set.all():
            workshop_registered_for.append(workshop.id)

    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:workshops',
                            {
                                'workshops': ws,
                                'workshop_registered_for': workshop_registered_for,
                            })

    elif request.method == 'POST':
        workshop = Workshop.objects.get(id=int(request.POST['workshop_id']))
        workshop.registered_users.add(request.user)
        workshop.save()

        workshop_registered_for.append(workshop.id)

        if request.POST['vnit_id'] != "" or request.POST['vnit_id'] is not None:
            u = request.user
            u.vnit_id = request.POST['vnit_id']
            u.save()

        messages.success(request, "Successfully Registered for workshop " + workshop.name + "!")

        return render(request, 'axis/workshops.html',
                      {
                          'user_registrations': UserRegistration(request.user),
                          'workshops': ws,
                          'workshop_registered_for': workshop_registered_for,
                      })
    else:
        return render(request, 'axis/workshops.html',
                      {
                          'user_registrations': UserRegistration(request.user),
                          'workshops': ws,
                          'workshop_registered_for': workshop_registered_for,
                      })


'''
def event(request, cat_slug, event_slug):
    e = Event.objects.get(slug=event_slug)
    if request.method == 'POST':
        m = 'member_email'
        mail_list = []
        team_name = request.POST['team_name']
        for i in range(2, e.max_team_mem+1):
            email = request.POST[m + str(i)]
            if email:
                mail_list.append(email)
        t = Team()
        t.team_name = team_name
        t.event = e
        t.save()
        t.users.add(request.user)
        t.save()
        for idd in mail_list:
            if request.user.email != idd:
                r = Request()
                r.team_id = t
                r.user_who_requested = str(request.user.first_name) + ' ' + str(request.user.last_name)
                r.requested_user = MyUser.objects.get(email=idd)
                r.save()
                # u = url('axis:events') + str(e.category.slug)
        return redirect('axis:home')
    return render(request, 'axis/event.html', {'event': e, 'range': range(2, e.max_team_mem+1), })


def requests(request):
    req = Request.objects.filter(requested_user=request.user)
    # req_dict = dict()
    # for r in req:
    #     team = Team.objects.get(pk=r.team_id.pk)
    #     req_dict.update({r: team})
    return render(request, 'axis/requests.html', {'req': req, })


def accept_request(request, team_id):

    team = Team.objects.get(pk=team_id)
    try:
        my_team = Team.objects.get(event=team.event, users__in=[request.user])
        my_team.users.remove(request.user)
        my_team.save()
        team.users.add(request.user)
        team.save()
    except Team.DoesNotExist:
        team.users.add(request.user)
        team.save()
    req = Request.objects.get(requested_user=request.user, team_id=team)
    req.delete()
    return redirect('axis:home')


def reject_request(request, req_id):
    req = Request.objects.get(pk=req_id)
    req.delete()
    return redirect('axis:home')
'''


def send_validation_email(user, link, password):
    print('idhar aaya mai')
    link = link + '/validate_email/' + str(urlsafe_base64_encode(force_bytes(user.pk)).decode('utf-8')) + '-' \
           + str(default_token_generator.make_token(user))
    html_msg = loader.render_to_string('axis/email.html', {'name': user.f_name, 'link': link,
                                                           'axis_id': user.axis_id, 'email': user.email,
                                                           'password': password})
    # send_mail('E-mail confirmation-' + user.axis_id, 'Hello', EMAIL_HOST_USER,
    #           ['atharva.parwatkar@gmail.com'], html_message=html_msg,
    #           fail_silently=False)
    send_mail('E-mail confirmation-' + user.axis_id, 'Hello', EMAIL_HOST_USER,
              [user.email], html_message=html_msg,
              fail_silently=False)
    # counter = Counter.objects.get(name='email')
    # counter.count = counter.count + 1
    # if counter.count % 400 == 2:
    #     send_mail('EMAIL OVERLOAD', 'SMTP Daily Free tier about to be exhausted!!', EMAIL_HOST_USER,
    #               ['atharva.parwatkar@gmail.com'], fail_silently=False)
    # counter.save()
    print('gaya mail')


def event_registration_email(user, eventName, teamName=None, password=None):
    html_msg = loader.render_to_string('axis/eventRegistrationEmail.html', {'name': user.f_name,
                                                                            'eventname': eventName,
                                                                            'teamName': teamName,
                                                                            'password': password})

    send_mail('Registration successful for ' + eventName, 'Hello', EMAIL_HOST_USER,
              [user.email], html_message=html_msg,
              fail_silently=False)
    # counter = Counter.objects.get(name='email')
    # counter.count = counter.count + 1
    # if counter.count % 400 == 399:
    #     send_mail('EMAIL OVERLOAD', 'SMTP Daily Free tier about to be exhausted!!', EMAIL_HOST_USER,
    #               ['atharva.parwatkar@gmail.com'], fail_silently=False)
    # counter.save()


def validate_email(request, uidb64, token):
    assert uidb64 is not None and token is not None
    uid = urlsafe_base64_decode(uidb64)
    print(uid)
    user = MyUser.objects.get(pk=uid)
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        user.backend = 'django.db.backends.sqlite3'
        login(request, user)
        return redirect("axis:home")

    else:
        return HttpResponse('404 page not found!')


def updates(request):
    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:updates',
                            {
                                'user_registrations': UserRegistration(request.user),
                                'time_line_objects': TimeLine.objects.all().order_by('-date')
                            })
    else:
        return render(request, 'axis/updates.html',
                      {
                          'user_registrations': UserRegistration(request.user),
                          'time_line_objects': TimeLine.objects.all().order_by('-date')
                      })


def sponsors(request):
    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:sponsors',
                            {
                                'user_registrations': UserRegistration(request.user),
                            })
    else:
        return render(request, 'axis/sponsors.html',
                      {
                          'user_registrations': UserRegistration(request.user),
                      })


def attractions(request):
    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:attractions',
                            {
                                'user_registrations': UserRegistration(request.user),
                            })
    else:
        return render(request, 'axis/attractions.html',
                      {
                          'user_registrations': UserRegistration(request.user),
                      })


def talks(request):
    all_talks = Talks.objects.all()
    talks_registered_for = []

    if request.user.is_authenticated:
        for talk in request.user.talks_set.all():
            talks_registered_for.append(talk.id)

    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:talks',
                            {'all_talks': all_talks, 'talks_registered_for': talks_registered_for})

    elif request.method == 'POST':
        talk = Talks.objects.get(id=request.POST['talkid'])

        if request.user not in talk.user.all():
            talk.user.add(request.user)
            talk.save()
            # request.session['message'] = "Successfully Registered for " + talk.name + '\'s talk.'
            # request.session['message_type'] = "success"
            return redirect(talk.link)

    context = {'all_talks': all_talks, 'talks_registered_for': talks_registered_for}
    return render(request, 'axis/talks.html', context)


def guestlectures(request):
    if request.method == 'POST' and 'question' in request.POST:
        gl = GuestLectures.objects.get(id=request.POST['lectureid'])

        if request.user not in gl.user.all():
            gl.user.add(request.user)
            gl.save()
            messages.success(request, "Successfully Registered for " + gl.speaker_name + '\'s lecture.')

        if request.POST['question'] != "":
            qgl = GuestLectureQuestions()
            qgl.text = request.POST['question']
            qgl.guest_lecture = gl
            qgl.user = request.user
            qgl.save()
            messages.success(request, "Question submitted for " + gl.speaker_name + '\'s lecture.')

    guest_lectures = GuestLectures.objects.all()

    lec_registered_for = []

    if request.user.is_authenticated:
        for gl in request.user.guestlectures_set.all():
            lec_registered_for.append(gl.id)

    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:guestlectures',
                            {
                                'guest_lectures': guest_lectures,
                                'lec_registered_for': lec_registered_for,
                            })
    else:
        return render(request, 'axis/guest_lectures.html',
                      {
                          'user_registrations': UserRegistration(request.user),
                          'guest_lectures': guest_lectures,
                          'lec_registered_for': lec_registered_for,
                      })


def initiatives(request):
    social_initiatives = SocialInitiative.objects.all()

    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:social_initiatives', {'social_initiatives': social_initiatives})
    else:
        return render(request, 'axis/social_initiative.html',
                      {'user_registrations': UserRegistration(request.user), 'social_initiatives': social_initiatives})


def exhibitions(request):
    exhibition = Exhibition.objects.all()

    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:exhibition', {'exhibitions': exhibition})
    else:
        return render(request, 'axis/exhibitions.html',
                      {'user_registrations': UserRegistration(request.user), 'exhibitions': exhibition})


def inauguration(request):
    inaugurations = Inauguration.objects.all()

    if request.method == 'POST' and ('Login' in request.POST or 'Signup' in request.POST):
        return login_signup(request, 'axis:inauguration', {'inaugurations': inaugurations})

    elif request.method == 'POST' and ('register' in request.POST):
        for inaug in inaugurations:
            inaug.user.add(request.user)
            inaug.save()
            messages.success(request, "Successfully registered for Inauguration!")
            return redirect('axis:inauguration')

    elif request.method == 'POST' and ('submit_question' in request.POST) and (request.POST['question'] != ''):
        text = request.POST['question']
        inaug_question = InaugurationQuestions()
        inaug_question.user = request.user
        inaug_question.text = text
        inaug_question.save()
        messages.info(request, "Question Submitted!")
        return redirect('axis:inauguration')

    else:
        is_registered = False
        is_closed = False
        if request.user.is_authenticated:
            user = request.user
            for inaug in inaugurations:
                if inaug.registrations_closed:
                    is_closed = True
                if inaug in user.inauguration_set.all():
                    is_registered = True

        return render(request, 'axis/inaug.html',
                      {'inaugurations': inaugurations,
                       'is_registered': is_registered, 'is_closed': is_closed})


def not_found(request):
    return render(request, 'axis/not_found.html', {'user_registrations': UserRegistration(request.user)})


def google_site_verification(request):
    return render(request, 'axis/googleb1165711f14fb6e5.html')


def sitemap(request):
    return render(request, 'axis/sitemap.xml', content_type='application/xhtml+xml')


def tshirt(request):
    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:tshirt')
    else:
        return render(request, 'axis/tshirt.html')


def home(request):
    print("in home")
    print(request.POST)
    # if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, "axis:home")
    else:
        print("in home else")
        current_updates = CurrentUpdates.objects.all()

        return render(request, 'axis/home.html',
                      {
                          'user_registrations': UserRegistration(request.user),
                          "current_updates": current_updates
                      })


def login_signup(request, page, context={}):
    print("in login signup")
    if 'password' in request.POST:
        print("in login")
        email = request.POST['email']
        password = request.POST['password']
        flag = True
        # u = get_object_or_404(MyUser, email=email)
        try:
            u = MyUser.objects.get(email=email)
        except:
            messages.error(request, 'Invalid Credentials.')
            return redirect(page)
        print("after exvept")
        '''
        if u is None:
            request.session['message'] = "Invalid Credentials"
            request.session['message_type'] = "error"
            __context = {"message_type": request.session['message_type'],
                                                      "message": request.session['message'],
                                                    }
            __context.update(context)
            return render(request, page, __context)
        '''
        if not u.is_active:
            print(u.f_name)
            flag = False
            u.is_active = True
            u.save()

        user = authenticate(email=email, password=password)
        # print(user.l_name)
        if not flag:
            # u = MyUser.objects.get(email=email)
            print(user.axis_id)
            user.is_active = False
            user.save()
            flag = True
        # print(user.f_name)
        if user is not None:
            if user.is_active:
                # print("active")
                login(request, user)
                # messages.warning(request, "remove_hash")
                messages.success(request, "You've been successfully logged in!")
                return redirect(str(request.path) + '#')
            else:
                # print("not active!!!!!!")
                messages.info(request, "Please validate your account first.")
                return redirect(page)

        else:
            messages.error(request, 'Invalid Credentials.')
            return redirect(page)

    elif "password1" in request.POST:
        form = UserCreationForm(request.POST or None)
        print(form.errors)
        if form.is_valid():
            user = form.save(commit=False)
            email = form.cleaned_data['email']
            password = form.cleaned_data['password1']
            user.set_password(password)
            user.date_created = datetime.now()
            user.is_active = True
            user.is_admin = False
            user.save()
            user.axis_id = 'AXIS18' + (str(user.pk + 4235).zfill(5))
            user.save()
            # print(user.axis_id)
            # link = request.META['HTTP_HOST']
            # send_validation_email(user, link, password)
            # messages.warning(request, "remove_hash")
            messages.success(request, "Successfully Registered! Please login to continue.")
            return redirect(str(request.path) + '#')

        else:
            messages.error(request, "Email Id already exists! Please try signing up using different email address.")
            # current_updates = CurrentUpdates.objects.all()
            #
            # return render(request, 'axis/home.html',
            #               {
            #                   'user_registrations': UserRegistration(request.user),
            #                   "current_updates": current_updates
            #               })
            return redirect(page)


@api_view(['GET'])
def event_list(request):
    if request.method == 'GET':
        events = Event.objects.all()
        serializer = EventSerializer(events, many=True)
        return Response(serializer.data)


def techie_nite(request):
    tech = TechieNite.objects.all()

    if request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, 'axis:exhibition', {'tech': tech})
    else:
        return render(request, 'axis/techie_nite.html',
                      {'user_registrations': UserRegistration(request.user), 'tech': tech})


@api_view(['GET'])
def event_category_list(request):
    if request.method == 'GET':
        event_categories = EventCategory.objects.all()
        # event_categories = EventCategory.objects.filter(pk=1)
        # print(event_categories)
        serializer = EventCategorySerializer(event_categories, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def workshops_list(request):
    if request.method == 'GET':
        workshops = Workshop.objects.all().order_by('-id')
        serializer = WorkshopSerializer(workshops, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def guest_lectures_list(request):
    if request.method == 'GET':
        guest_lectures = GuestLectures.objects.all().order_by('-id')
        serializer = GuestLecturesSerializer(guest_lectures, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def social_initiatives_list(request):
    if request.method == 'GET':
        social_initiatives = SocialInitiative.objects.all().order_by('-id')
        serializer = SocialInitiativeSerializer(social_initiatives, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def exhibitions_list(request):
    if request.method == 'GET':
        exhibitions = Exhibition.objects.all().order_by('-id')
        serializer = ExhibitionSerializer(exhibitions, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def talks_list(request):
    if request.method == 'GET':
        talks = Talks.objects.all().order_by('-id')
        serializer = TalksSerializer(talks, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def timeline_list(request):
    if request.method == 'GET':
        timeline = TimeLine.objects.all()
        serializer = TimeLineSerializer(timeline, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def current_updates_list(request):
    if request.method == 'GET':
        current_updates = CurrentUpdates.objects.all()
        serializer = CurrentUpdatesSerializer(current_updates, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def schedule_list(request, day_number):
    if request.method == 'GET':
        schedule = Schedule.objects.filter(date__day=(22 + int(day_number))).order_by('date')
        serializer = ScheduleSerializer(schedule, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def inauguration_list(request):
    if request.method == 'GET':
        inauguration_speakers = Inauguration.objects.all().order_by('id')
        serializer = InaugurationSerializer(inauguration_speakers, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def summit_list(request):
    if request.method == 'GET':
        summit_speakers = Summit.objects.all().order_by('id')
        serializer = SummitSerializer(summit_speakers, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def techie_nite_list(request):
    if request.method == 'GET':
        objs = TechieNite.objects.all().order_by('id')
        serializer = TechieNiteSerializer(objs, many=True)
        return Response(serializer.data)


@api_view((['POST']))
@csrf_exempt
# @authentication_classes((SessionAuthentication, BasicAuthentication))
# @permission_classes((IsAuthenticated,))
def api_login(request):
    if request.method == 'POST':
        request_serializer = MyUserLoginSerializer(data=request.data)
        # return Response({"message": "hihihihihi"})
        # print(str(request_serializer))
        if request_serializer.is_valid():
            # print ('haha serializer is' + str(request_serializer))
            #     email = request_serializer.data['email']
            #     password = request_serializer.data['password']
            #     # print('hihi ' + str(email))
            #     user = authenticate(email=email, password=password)
            #     if user is not None:
            #         if user.is_active:
            #             # print('pela pela pela!!!')
            #             login(request, user)
            #             response_serializer = MyUserLoginResponseSerializer()
            #             response_serializer.data['error'] = 'False'
            #             response_serializer.data['message'] = ''
            #             # return Response({'error': 'False'})
            #             return Response(response_serializer.data)
            # return Response({'error': 'True'})
            email = request_serializer.data['email']
            password = request_serializer.data['password']
            # response_serializer = MyUserLoginResponseSerializer('True', 'hihi')
            flag = True
            # u = get_object_or_404(MyUser, email=email)
            try:
                u = MyUser.objects.get(email=email)
            except ObjectDoesNotExist:
                print('hihihi')
                # response_serializer.data['message'] = "Invalid Credentials"
                # response_serializer.data['error'] = "True"
                return Response({"error": "True", "message": "Invalid Credentials"})

            if not u.is_active:
                print(u.f_name)
                flag = False
                u.is_active = True
                u.save()

            user = authenticate(email=email, password=password)
            print(password)
            # print(user.l_name)
            if not flag:
                # u = MyUser.objects.get(email=email)
                print(user.axis_id)
                user.is_active = False
                user.save()
                flag = True
            # print(user.f_name)
            if user is not None:
                if user.is_active:
                    print("active")
                    login(request, user)
                    # response_serializer.data['error'] = "True"
                    # response_serializer.data['message'] = "You've been successfully logged in!"
                    print('haha')
                    # logout(user)
                    # resp = dict()
                    # resp['error'] = False
                    # resp['message'] = "You've been successfully logged in!"
                    # respose = MyUserLoginResponseSerializer(data=resp)
                    # print(respose.data)
                    return Response({"error": "False", "message": "You've been successfully logged in!",
                                     "f_name": user.f_name, "axis_id": user.axis_id, "email": user.email})
                else:
                    print("not active!!!!!!")
                    # response_serializer.data['message'] = "Please validate your account first!"
                    # response_serializer.data['error'] = "True"
                    return Response({"error": "True", "message": "Please validate your account first!  "})

            else:
                # print("hahaha")
                # response_serializer.data['message'] = "Invalid Credentials"
                # response_serializer.data['error'] = "True"
                return Response({"error": "True", "message": "Invalid Credentials"})


@api_view((['POST']))
def api_signup(request):
    if request.method == 'POST':
        # print('haha')
        request_serializer = UserCreationSerializer(data=request.data)
        if request_serializer.is_valid():
            # print(request_serializer.data)
            form = UserCreationForm(request_serializer.data or None)
            # response_serializer = UserCreationResponseSerializer()
            if form.is_valid():
                print('hihi')
                user = form.save(commit=False)
                email = form.cleaned_data['email']
                password = form.cleaned_data['password1']
                user.set_password(password)
                user.date_created = datetime.now()
                user.is_active = True
                user.is_admin = False
                user.save()
                user.axis_id = 'AXIS18' + (str(user.pk + 4235).zfill(5))
                user.save()
                # print(user.axis_id)
                # link = request.META['HTTP_HOST']
                # print(link)
                # send_validation_email(user, link, password)
                # send_mail('haha', 'hihi', EMAIL_HOST_USER, ['atharva.parwatkar@gmail.com'], fail_silently=False)
                # response_serializer.data['error'] = 'False'
                # response_serializer.data['message'] = 'Successfully Registered! Please check your mail to validate
                # your account!! '
                # resp = dict()
                # resp['error'] = 'False'
                # resp['message'] = 'Successfully Registered! Please check your mail to validate your account!!'
                return Response({"error": "False", "message": "Successfully Registered! Please login to continue."})
                # return Response(str(resp))
            # response_serializer.data['error'] = 'True'
            # response_serializer.data['message'] = 'Email Id already exists! Please try signing up using different email ' \
            #                                       'address. '
            return Response({"error": "True", "message": "Email Id already exists! Please try signing up using different email ' \
                                                  'address."})
        return Response({"error": "True", "message": "Something went wrong!"})


@api_view(['POST'])
def api_register_single(request):
    if request.method == 'POST':
        serializer = SingleEventRegistration(data=request.data)
        if serializer.is_valid():
            axis_id = serializer.data['axis_id']
            event_id = serializer.data['event_id']
            user = MyUser.objects.get(axis_id=axis_id)
            try:
                SingleParticipantEventRegistration.objects.get(user=user, event=Event.objects.get(id=event_id))
                return Response({"error": "True",
                                 "message": str('You are already registered for '
                                                + Event.objects.get(id=event_id).name)})
                # messages.info(request, 'You are already registered for ' + Event.objects.get(id=event_id).name)
            except ObjectDoesNotExist:
                try:
                    registration = SingleParticipantEventRegistration()
                    registration.user = user
                    registration.event = Event.objects.get(id=event_id)
                    registration.save()
                    # event_registration_email(user, Event.objects.get(id=event_id).name)
                    return Response({"error": "False",
                                     "message": str("Successfully registered for " + registration.event.name)})
                    # messages.success(request, "Successfully registered for " + registration.event.name)
                except ObjectDoesNotExist:
                    return Response({"error": "True", "message": "Event does not exist"})
                    # messages.success(request, "Event does not exist")


@api_view(['POST'])
def api_register_team(request):
    if request.method == 'POST':
        serializer = TeamEventRegistration(data=request.data)
        if serializer.is_valid():
            type = serializer.data['type']
            axis_id = serializer.data['axis_id']
            __team_name = serializer.data['team_name']
            __team_password = serializer.data['team_password']
            event_id = serializer.data['event_id']
            user = MyUser.objects.get(axis_id=axis_id)
            if type == 'create':
                try:
                    # Check if team name is already taken
                    if Team.objects.get(team_name=__team_name):
                        return Response({"error": "True", "message": str(__team_name + " already taken.")})
                        # messages.success(request, __team_name + " already taken.")
                except ObjectDoesNotExist:
                    Team().set_values(__team_name, Event.objects.get(id=event_id), user, __team_password)
                    # event_registration_email(user, Event.objects.get(id=event_id).name, __team_name,
                    #                          __team_password)
                    return Response({"error": "False", "message": str("Successfully registered and created team " +
                                                                      __team_name + " for " +
                                                                      Event.objects.get(id=event_id).name)})
            elif type == 'join':
                try:
                    team = Team.objects.get(team_name=__team_name)

                    # Check if password matches then let user join the team
                    if team.check_password(__team_password) and team.event.id == int(event_id):
                        team.add_user(user)
                        # Todo: handle success response
                        # event_registration_email(user, Event.objects.get(id=event_id).name, __team_name,
                        #                          __team_password)
                        return Response({"error": "False", "message": str("You have successfully joined the team " +
                                                                          team.team_name + " for " + team.event.name)})
                        # messages.success(request,
                        #                  "You have successfully joined the team " + team.team_name + " for " +
                        # team.event.name)

                    else:
                        return Response({"error": "True", "message": str("Invalid Credentials to join a team for " +
                                                                         str(Event.objects.get(id=event_id).name))})
                        # messages.error(request,
                        #                'Invalid Credentials to join a team for' + Event.objects.get(id=event_id).name)
                        # return HttpResponse("Invalid Credentials")

                except ObjectDoesNotExist:
                    # Todo: handle failure response
                    return Response({"error": "True", "message": "Team does NOT exists!!!"})
                    # messages.error(request,
                    #                'Invalid Credentials to join a team for' + Event.objects.get(id=event_id).name)


@api_view(['POST'])
def api_register_workshop(request):
    if request.method == 'POST':
        serializer = WorkshopRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            workshop_id = serializer.data['workshop_id']
            axis_id = serializer.data['axis_id']
            user = MyUser.objects.get(axis_id=axis_id)
            workshop = Workshop.objects.get(id=workshop_id)
            if not workshop.registrations_closed:
                workshop.registered_users.add(user)
                workshop.save()

                # if request.POST['vnit_id'] != "" or request.POST['vnit_id'] is not None:
                #     u = request.user
                #     u.vnit_id = request.POST['vnit_id']
                #     u.save()
                return Response({"error": "False", "message": str("Successfully Registered for workshop " +
                                                                  workshop.name + "!")})
            else:
                return Response({"error": "True", "message": str("Registrations closed for workshop " +
                                                                 workshop.name + "!")})
            # messages.success(request, "Successfully Registered for workshop " + workshop.name + "!")
        return Response({"error": "True", "message": "Something went wrong!!!"})


@api_view(['GET'])
def is_registered_event(request, axis_id, event_id):
    try:
        event = Event.objects.get(id=event_id)
        user = MyUser.objects.get(axis_id=axis_id)
        if event.max_team_mem == 1:
            try:
                reg = SingleParticipantEventRegistration.objects.get(user=user, event=event)
                return Response({"is_registered": "True"})
            except ObjectDoesNotExist:
                return Response({"is_registered": "False"})
        else:
            try:
                team = Team.objects.get(event=event, users=user)
                return Response({"is_registered": "True"})
            except ObjectDoesNotExist:
                return Response({"is_registered": "False"})
    except ObjectDoesNotExist:
        return Response({"message": "Something went wrong"})


@api_view(['GET'])
def is_registered_workshop(request, axis_id, workshop_id):
    try:
        workshop = Workshop.objects.get(id=workshop_id)
        user = MyUser.objects.get(axis_id=axis_id)
        try:
            workshop = Workshop.objects.get(id=workshop_id, registered_users=user)
            return Response({"is_registered": "True"})
        except ObjectDoesNotExist:
            return Response({"is_registered": "False"})
    except ObjectDoesNotExist:
        return Response({"message": "Something went wrong"})


@api_view(['GET'])
def all_registered_event(request, axis_id):
    try:
        user = MyUser.objects.get(axis_id=axis_id)
        if user.is_anonymous:
            return None, None

        # registered_for = []
        registered_events_id_list = []

        # Extracting details of single participant event details
        for spe in SingleParticipantEventRegistration.objects.filter(user=user):
            # registered_for.append(RegisteredEventsDetails(spe.event.name, user.f_name + " " + user.l_name))
            registered_events_id_list.append(spe.event.id)

        # Extracting details of team event details
        for t in user.team_set.all():
            registered_events_id_list.append(t.event.id)

            # r = RegisteredEventsDetails(t.event.name, s, t.team_name)
            # registered_for.append(r)

        # return registered_events_id_list, registered_for
        # return Response(json.dumps(registered_events_id_list))
        return Response({"id_list": registered_events_id_list})
    except ObjectDoesNotExist:
        return Response({"message": "Something went wrong"})


@api_view(['GET'])
def all_registered_workshop(request, axis_id):
    try:
        user = MyUser.objects.get(axis_id=axis_id)
        ws = user.workshop_set.values('id')
        registered_workshops_id_list = list()
        for w in ws:
            # print(w)
            registered_workshops_id_list.append(w['id'])
        return Response({"id_list": registered_workshops_id_list})
    except ObjectDoesNotExist:
        return Response({"message": "Something went wrong"})


@api_view(['POST'])
def api_register_talk(request):
    if request.method == 'POST':
        axis_id = request.data['axis_id']
        talk_id = request.data['talk_id']
        try:
            user = MyUser.objects.get(axis_id=axis_id)
            talk = Talks.objects.get(id=talk_id)
            talk.user.add(user)
            talk.save()
            return Response({"error": "False", "message": "Successfully registered for " + str(talk.name)})
        except ObjectDoesNotExist:
            return Response({"error": "True", "message": "Something went wrong!"})


@api_view(['POST'])
def api_register_guest_lecture(request):
    if request.method == 'POST':
        axis_id = request.data['axis_id']
        guest_lecture_id = request.data['guest_lecture_id']
        try:
            user = MyUser.objects.get(axis_id=axis_id)
            guest_lecture = GuestLectures.objects.get(id=guest_lecture_id)
            guest_lecture.user.add(user)
            guest_lecture.save()
            return Response({"error": "False", "message": "Successfully registered for " +
                                                          str(guest_lecture.speaker_name)})
        except ObjectDoesNotExist:
            return Response({"error": "True", "message": "Something went wrong!"})


@api_view(['POST'])
def api_register_inauguration(request):
    if request.method == 'POST':
        axis_id = request.data['axis_id']
        try:
            user = MyUser.objects.get(axis_id=axis_id)
            inaug_speakers = Inauguration.objects.all()
            done = False
            for speaker in inaug_speakers:
                if not speaker.registrations_closed:
                    speaker.user.add(user)
                    speaker.save()
                    done = True
            if done:
                if 'question' in request.data:
                    inaug_question = InaugurationQuestions()
                    text = str(request.data['question']).strip(' ')
                    if text == "":
                        return Response({"error": "False", "message": "Successfully registered for Inauguration"})
                    else:
                        inaug_question.text = text
                        inaug_question.user = user
                        inaug_question.save()
                    return Response({"error": "False", "message": "Successfully registered for Inauguration"
                                                                  " with question"})
                else:
                    return Response({"error": "False", "message": "Successfully registered for Inauguration"})
            else:
                return Response({"error": "True", "message": "Registrations closed for Inauguration"})
        except ObjectDoesNotExist:
            return Response({"error": "True", "message": "Something went wrong!"})


@api_view(['GET'])
def is_registered_guest_lecture(request, axis_id, guest_lecture_id):
    try:
        user = MyUser.objects.get(axis_id=axis_id)
        try:
            guest_lecture = GuestLectures.objects.get(id=guest_lecture_id, user=user)
            return Response({"is_registered": "True"})
        except ObjectDoesNotExist:
            return Response({"is_registered": "False"})
    except ObjectDoesNotExist:
        return Response({"message": "Something went wrong"})


@api_view(['GET'])
def is_registered_talk(request, axis_id, talk_id):
    try:
        user = MyUser.objects.get(axis_id=axis_id)
        try:
            talk = Talks.objects.get(id=talk_id, user=user)
            return Response({"is_registered": "True"})
        except ObjectDoesNotExist:
            return Response({"is_registered": "False"})
    except ObjectDoesNotExist:
        return Response({"message": "Something went wrong"})


@api_view(['GET'])
def is_registered_inauguration(request, axis_id):
    # try:
    #     user = MyUser.objects.get(axis_id=axis_id)
    #     inauguration = Inauguration.objects.filter(user=user)
    #     if inauguration.first() is not None:
    #         return Response({"is_registered": "True"})
    #     else:
    #         return Response({"is_registered": "False"})
    # except ObjectDoesNotExist:
    #     return Response({"message": "Something went wrong"})
    inaugurations = Inauguration.objects.all()
    if not inaugurations:
        return Response({"message": "No summit in database!", "is_registered": "False"})
    else:
        try:
            user = MyUser.objects.get(axis_id=axis_id)
            if user.inauguration_set.all():
                return Response({"is_registered": "True"})
            else:
                return Response({"is_registered": "False"})
        except ObjectDoesNotExist:
            return Response({"message": "User does NOT exist!"})


@api_view(['POST'])
def api_register_summit(request):
    if request.method == 'POST':
        axis_id = request.data['axis_id']
        try:
            user = MyUser.objects.get(axis_id=axis_id)
            summit_speakers = Summit.objects.all()
            done = False
            for speaker in summit_speakers:
                if not speaker.registrations_closed:
                    speaker.user.add(user)
                    speaker.save()
                    done = True
            if done:
                return Response({"error": "False", "message": "Successfully registered for Summit"})
            else:
                return Response({"error": "True", "message": "Registrations closed for Summit"})
        except ObjectDoesNotExist:
            return Response({"error": "True", "message": "Something went wrong!"})


@api_view(['POST'])
def api_register_techie_nite(request):
    if request.method == 'POST':
        axis_id = request.data['axis_id']
        tc_id = request.data['tc_id']
        try:
            user = MyUser.objects.get(axis_id=axis_id)
            tc = TechieNite.objects.get(id=tc_id)
            if not tc.registrations_closed:
                tc.user.add(user)
                tc.save()
                return Response({"error": "False", "message": "Successfully registered for TechieNite"})
            else:
                return Response({"error": "True", "message": "Registrations closed for TechieNite"})
        except ObjectDoesNotExist:
            return Response({"error": "True", "message": "Something went wrong!"})


@api_view(['GET'])
def is_registered_summit(request, axis_id):
    # try:
    #     user = MyUser.objects.get(axis_id=axis_id)
    #     summit = Summit.objects.filter(user=user)
    #     if summit.first() is not None:
    #         return Response({"is_registered": "True"})
    #     else:
    #         return Response({"is_registered": "False"})
    # except ObjectDoesNotExist:
    #     return Response({"message": "Something went wrong"})
    summits = Summit.objects.all()
    if not summits:
        return Response({"message": "No summit in database!", "is_registered": "False"})
    else:
        try:
            user = MyUser.objects.get(axis_id=axis_id)
            if user.summit_set.all():
                return Response({"is_registered": "True"})
            else:
                return Response({"is_registered": "False"})
        except ObjectDoesNotExist:
            return Response({"message": "User does NOT exist!"})


@api_view(['GET'])
def all_registered_talks(request, axis_id):
    try:
        user = MyUser.objects.get(axis_id=axis_id)
        talks = user.talks_set.values('id')
        registered_taks_id_list = list()
        for talk in talks:
            # print(w)
            registered_taks_id_list.append(talk['id'])
        return Response({"id_list": registered_taks_id_list})
    except ObjectDoesNotExist:
        return Response({"message": "Something went wrong"})


@api_view(['GET'])
def all_registered_guest_lectures(request, axis_id):
    try:
        user = MyUser.objects.get(axis_id=axis_id)
        guest_lectures = user.guestlectures_set.values('id')
        registered_guest_lectures_id_list = list()
        for guest_lecture in guest_lectures:
            # print(w)
            registered_guest_lectures_id_list.append(guest_lecture['id'])
        return Response({"id_list": registered_guest_lectures_id_list})
    except ObjectDoesNotExist:
        return Response({"message": "Something went wrong"})
