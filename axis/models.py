from django.db import models
from django.db import models
from datetime import datetime
from django.template.defaultfilters import slugify
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

COMING_SOON_THUMBNAIL = 'coming-soon.png'
COMING_SOON_BACKGROUND = 'coming-soon.jpg'
DEFAULT_BACKGROUND_FOR_ACCORDION = 'coming-soon.jpg'


class MyUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email=email,
            # enr_no=enr_no,
            # id_no=id_no,
            # cgpa=cgpa,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        """"Does the user have permissions to view the app `app_label`?"""
        # Simplest possible answer: Yes, always
        return True


class MyUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    f_name = models.CharField(max_length=50, null=True)
    l_name = models.CharField(max_length=50, null=True)
    # username = models.CharField(max_length=50, null=True, blank=True)
    contact_no = models.CharField(max_length=20, null=True)
    clg_name = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    year = models.CharField(max_length=10, default='3rd')
    axis_id = models.CharField(max_length=12, default='AXIS1800001')
    vnit_id = models.CharField(max_length=10, null=True, default=None, blank=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        # The user is identified by their email address
        return self.f_name + self.l_name

    def get_short_name(self):
        # The user is identified by their email address
        return self.f_name

    # @property
    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        """"Does the user have permissions to view the app `app_label`?"""
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        """"Is the user a member of staff?"""
        # Simplest possible answer: All admins are staff
        return self.is_admin


class EventCategory(models.Model):
    name = models.CharField(max_length=50, null=True)
    slug = models.SlugField(null=True, blank=True)
    icon = models.CharField(max_length=100, default='<i class="fa fa-building-o fa-4x" style="color: white; margin-top: 20px" aria-hidden="true"></i>')
    # MyUser.objects.all()[0].workshop_set
    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(EventCategory, self).save(*args, **kwargs)


class Event(models.Model):

    name = models.CharField(max_length=500)
    slug = models.SlugField(null=True, blank=True)
    max_team_mem = models.IntegerField(null=False, default=1)
    description = models.CharField(max_length=50000, default='To be updated soon...')
    rules = models.CharField(max_length=50000, default='To be updated soon...')
    prob_stmt = models.CharField(max_length=50000, default='To be updated soon...')
    event_managers = models.CharField(max_length=10000, default="")
    date = models.DateTimeField()
    last_date_reg = models.DateTimeField()
    category = models.ForeignKey(EventCategory, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def is_single_member(self):
        return self.max_team_mem == 1

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Event, self).save(*args, **kwargs)


class Team(models.Model):
    team_name = models.CharField(max_length=50, unique=True, null=True)
    users = models.ManyToManyField(MyUser, blank=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    password = models.CharField(max_length=1000, null=False, default="qwer1234")

    def set_values(self, __team_name, _event_id, __user, __password):
        self.team_name = __team_name
        self.event = _event_id
        self.save()
        self.users.add(__user)
        self.set_password_and_save_team(__password)

    def set_password_and_save_team(self, __password):
        # Todo: hash password
        self.password = __password
        self.save()

    def add_user(self, user):
        if self.event.max_team_mem > self.users.count():
            self.users.add(user)
            self.save()
            return True
        else:
            return False

    def check_password(self, __password):
        if __password == self.password:
            return True
        else:
            return False

    def __str__(self):
        return str(self.team_name)


class SingleParticipantEventRegistration(models.Model):
    user = models.ForeignKey(MyUser, null=False, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, null=False, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user) + " Registered for " + str(self.event)


class Request(models.Model):
    requested_user = models.ForeignKey(MyUser, on_delete=models.CASCADE, null=True)
    team_id = models.ForeignKey(Team, on_delete=models.CASCADE)
    user_who_requested = models.CharField(max_length=100, null=True)

    def __str__(self):
        return str(str(self.requested_user.f_name) + ' was requested by ' + self.user_who_requested)


class CurrentUpdates(models.Model):
    update = models.CharField(max_length=100, null=True)
    link = models.CharField(max_length=50, default='#')

    def __str__(self):
        return str(self.update)


class TimeLine(models.Model):
    date = models.DateField()
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)

    def __str__(self):
        return self.title


class GuestLectures(models.Model):
    speaker_name = models.CharField(max_length=100, default="Not Known")
    about_speaker = models.CharField(max_length=3000)
    about_lecture = models.CharField(max_length=3000)
    venue = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    speaker_image = models.FileField(upload_to="guest_lectures_images", )
    user = models.ManyToManyField(MyUser, blank=True)
    isvalid = models.BooleanField(default=True)

    def __str__(self):
        return self.speaker_name


class GuestLectureQuestions(models.Model):
    text = models.CharField(max_length=1000, verbose_name='question')
    guest_lecture = models.ForeignKey(GuestLectures)
    user = models.ForeignKey(MyUser)

    def __str__(self):
        return self.text


class Workshop(models.Model):
    name = models.CharField(max_length=50, default="Coming Soon")
    description = models.CharField(max_length=1000, default="Coming Soon")
    image = models.FileField(default=COMING_SOON_THUMBNAIL, upload_to="workshop_images/thumbnail")
    background_image = models.FileField(default=COMING_SOON_BACKGROUND, upload_to="workshop_images/backgroung")
    registrations_closed = models.BooleanField(default=False)
    registered_users = models.ManyToManyField(MyUser, blank=True)
    payment_link = models.CharField(max_length=500, null=True, default=None, blank=True)

    def __str__(self):
        return self.name


class SocialInitiative(models.Model):
    name = models.CharField(max_length=50, default="Coming Soon")
    description = models.CharField(max_length=1000, default="Coming Soon")
    image = models.FileField(default=COMING_SOON_THUMBNAIL, upload_to="initiative_images/thumbnail")
    background_image = models.FileField(default=COMING_SOON_BACKGROUND, upload_to="initiative_images/backgroung")

    def __str__(self):
        return self.name


class Exhibition(models.Model):
    name = models.CharField(max_length=50, default="Coming Soon")
    description = models.CharField(max_length=10000, default="Coming Soon")
    venue = models.CharField(max_length=100, default='Not Decided')
    date = models.DateField()
    time = models.TimeField()
    image = models.FileField(default=COMING_SOON_THUMBNAIL, upload_to="exhibition_images/thumbnail")
    background_image = models.FileField(default=COMING_SOON_BACKGROUND, upload_to="exhibition_images/background")

    def __str__(self):
        return self.name


class Talks(models.Model):
    name = models.CharField(max_length=50, default="Coming Soon")
    description = models.CharField(max_length=1000, default="Coming Soon")
    venue = models.CharField(max_length=100, default='Not Decided')
    date = models.DateField()
    time = models.TimeField()
    image = models.FileField(default=COMING_SOON_THUMBNAIL, upload_to="talks_images/thumbnail")
    user = models.ManyToManyField(MyUser, blank=True)
    link = models.CharField(max_length=300, default='')

    def __str__(self):
        return self.name


class Schedule(models.Model):
    name = models.CharField(max_length=50)
    date = models.DateTimeField()
    venue = models.CharField(max_length=300)

    def __str__(self):
        return self.name


class Inauguration(models.Model):
    registrations = True
    guest_name = models.CharField(max_length=200)
    desc = models.CharField(max_length=2000)
    image = models.FileField(default=COMING_SOON_THUMBNAIL, upload_to="inauguration_images")
    registrations_closed = models.BooleanField(default=True)
    user = models.ManyToManyField(MyUser, blank=True)

    def __str__(self):
        return self.guest_name

    def save(self, *args, **kwargs):
        Inauguration.objects.all().update(registrations_closed=self.registrations_closed)
        super(Inauguration, self).save(*args, **kwargs)


class InaugurationQuestions(models.Model):
    text = models.CharField(max_length=1000, verbose_name='question')
    # guest_lecture = models.ForeignKey(GuestLectures)
    user = models.ForeignKey(MyUser)

    def __str__(self):
        return self.text


class Summit(models.Model):
    speaker_name = models.CharField(max_length=200)
    desc = models.CharField(max_length=2000)
    image = models.FileField(default=COMING_SOON_THUMBNAIL, upload_to="summit_images")
    venue = models.CharField(max_length=100, default="None")
    date = models.DateTimeField(null=True, blank=True)
    registrations_closed = models.BooleanField(default=True)
    user = models.ManyToManyField(MyUser, blank=True)

    def __str__(self):
        return self.speaker_name

    def save(self, *args, **kwargs):
        Summit.objects.all().update(registrations_closed=self.registrations_closed)
        super(Summit, self).save(*args, **kwargs)


class TechieNite(models.Model):
    name = models.CharField(max_length=100)
    desc = models.CharField(max_length=2000)
    image = models.FileField(default=COMING_SOON_THUMBNAIL, upload_to="techie_nite_images")
    venue = models.CharField(max_length=100, default="None")
    date = models.DateTimeField(null=True, blank=True)
    registrations_closed = models.BooleanField(default=True)
    user = models.ManyToManyField(MyUser, blank=True)

    def __str__(self):
        return self.name
