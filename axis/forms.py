from django import forms


class EmailForm(forms.Form):
    body = forms.CharField(
        max_length=5000,
        required=False,
        widget=forms.Textarea,
    )

    subject = forms.CharField(
        max_length=100,
        required=True,
        help_text='Enter a subject for the email?',
    )
    # email_body_template = 'email/account/withdraw.txt'

    field_order = (
        'subject',
        'body',
    )
