from django.apps import AppConfig


class BakerstreetConfig(AppConfig):
    name = 'bakerstreet'
