from django.db import models

from axis.models import MyUser

import datetime

COMING_SOON_THUMBNAIL = 'coming-soon.png'


class Test(models.Model):
    round = models.IntegerField(null=False, default=1)
    test_date = models.DateField()
    test_running = models.BooleanField(default=False)
    max_questions = models.IntegerField(null=False, default=10)

    def __str__(self):
        return "Round " + str(self.round)


class Question(models.Model):
    test = models.ForeignKey(Test, null=False, on_delete=models.CASCADE)
    level = models.IntegerField(default=1)
    image1 = models.FileField(upload_to="bakerstreet_images/", default=COMING_SOON_THUMBNAIL)
    image2 = models.FileField(upload_to="bakerstreet_images/", default=COMING_SOON_THUMBNAIL)
    image3 = models.FileField(upload_to="bakerstreet_images/", default=COMING_SOON_THUMBNAIL)
    # hint = models.CharField(max_length=1000, default="No hints are added yet. Be patient suckahh...")
    answer = models.CharField(max_length=1000, null=True)

    def __str__(self):
        return str(self.test) + " : " + "Question " + str(self.level)


class UserMetadata(models.Model):
    time_taken = models.DateTimeField(auto_now=True)
    questions_solved = models.IntegerField(null=False, default=0)
    user = models.ForeignKey(MyUser, null=False, on_delete=models.CASCADE)
    test = models.ForeignKey(Test, null=False, on_delete=models.CASCADE)

    # def get_top_scorers(self, test_number):
    #
    #     user_set = UserMetadata.objects.filter(test=test_number).order_by('-questions_solved')
    #     length = len(user_set)
    #     length = min(length, 10)
    #     return user_set[:length]

    def __str__(self):
        return "Round " + str(self.test.round) + " : " + str(self.user.f_name) + " - " + str(self.questions_solved)


class Hint(models.Model):
    test = models.ForeignKey(Test, null=True, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, null=True, on_delete=models.CASCADE)
    hint = models.CharField(max_length=1000, null=True)
    is_shown = models.BooleanField(default=False)

    def __str__(self):
        return "Question " + str(self.question.level) + " : " + str(self.hint)


