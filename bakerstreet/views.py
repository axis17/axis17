from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from axis.models import MyUser, Event, SingleParticipantEventRegistration
from axis.views import login_signup
from bakerstreet.models import UserMetadata, Test, Question, Hint
from django.contrib import messages
import json
import datetime

EVENT_ID = 14


def rules(request):
    if request.method == 'GET':
        return render(request, 'bakerstreet/bakerstreet_rules.html')
    elif request.method == 'POST' and ('Login' in request.POST or 'Signup' in request.POST):
        return login_signup(request, "bakerstreet:home")
    elif request.method == 'POST' and 'PLAY' in request.POST:
        register_for_bakerstreet(request)
        return redirect('bakerstreet:home')


# @login_required(login_url='/#login')
def home(request):

    try:
        test = Test.objects.get(test_running=True)
    except:
        return redirect('bakerstreet:time_completed')

    if not is_registered_for_bakerstreet(request):
        return redirect('bakerstreet:rules')

    if request.method == 'POST' and 'submit_answer' in request.POST:

        test = Test.objects.get(test_running=True)
        user_metadata = UserMetadata.objects.get(test=test, user=request.user)
        level = user_metadata.questions_solved + 1
        answer = request.POST['answer']
        correct_answer = Question.objects.get(test=test, level=level).answer

        answer = str(answer).lower()

        if answer == correct_answer:
            user_metadata.questions_solved += 1
            user_metadata.time_taken = datetime.datetime.now()
            user_metadata.save()

            if user_metadata.questions_solved == test.max_questions:
                print("completed!")
                return redirect('bakerstreet:test_completed')

            messages.success(request, "Correct answer, great job!")
            return redirect('bakerstreet:home')

        else:
            messages.info(request, "Wrong Answer! Please try again.")
            return redirect('bakerstreet:home')

    test = Test.objects.get(test_running=True)
    # print(test.round)

    try:
        user_metadata = UserMetadata.objects.get(user=request.user, test=test)

    except:

        user_metadata = UserMetadata.objects.create(test=test, user=request.user)
        # user_metadata.test = test
        # user_metadata.user = request.user
        user_metadata.save()

    level = user_metadata.questions_solved + 1
    if user_metadata.questions_solved == test.max_questions:
        print("completed!")
        return redirect('bakerstreet:test_completed')

    question = Question.objects.get(level=level, test=test)
    hints = Hint.objects.filter(question=question, is_shown=True)
    return render(request, 'bakerstreet/bakerfinal.html', context={"question": question, "hints": hints})


@login_required(login_url='/#login')
def test_completed(request):

    try:
        test = Test.objects.get(test_running=True)
        print("sdff")
    except:
        # print("dfgffd")
        return redirect('bakerstreet:time_completed')

    try:
        user_metadata = UserMetadata.objects.get(user=request.user, test=test)
    except:
        user_metadata = UserMetadata.objects.create(test=test, user=request.user)
        # user_metadata.test = test
        # user_metadata.user = request.user
        user_metadata.save()

    print("dfd")
    if user_metadata.questions_solved != test.max_questions:
        print("sdfd")
        return redirect('bakerstreet:home')

    return render(request, 'bakerstreet/Thank_you.html')


@csrf_exempt
def ajax_leaderboard(request):

    try:
        test = Test.objects.get(test_running=True)
    except:
        return redirect('bakerstreet:time_completed')

    leaderboard = test.usermetadata_set.order_by('-questions_solved', 'time_taken').\
        values('questions_solved', 'user__f_name', 'user__l_name', 'user__axis_id',)

    # print(request.user)
    # print(test)

    user_metadata = UserMetadata.objects.get(user=request.user, test=test)

    my_rank = int()
    print("ajax_leaderboard" + str(user_metadata))
    for i in range(len(leaderboard)):
        if user_metadata.user.axis_id == leaderboard[i]['user__axis_id']:
            my_rank = i
            my_rank_obj = leaderboard[i]
            print("my rank %s", my_rank)

    # my_rank_obj = leaderboard[my_rank]
    # print(my_rank_obj)
    length = min(len(leaderboard), 10)
    leaderboard = list(leaderboard[:length])
    leaderboard.append(my_rank_obj)

    print(leaderboard)

    return HttpResponse(
        json.dumps({"leaderboard": leaderboard, 'my_rank': my_rank + 1}), content_type='application/json'
    )


def register_for_bakerstreet(request):
    try:
        SingleParticipantEventRegistration.objects.get(user=request.user, event_id=EVENT_ID)
    except:
        SingleParticipantEventRegistration.objects.create(user=request.user, event_id=EVENT_ID)

    return


def is_registered_for_bakerstreet(request):
    try:
        SingleParticipantEventRegistration.objects.get(user=request.user, event_id=EVENT_ID)
        return True
    except:
        return False


def time_completed(request):

    try:
        test = Test.objects.get(test_running=True)
        return redirect('bakerstreet:home')
    except:
        return render(request, 'bakerstreet/time_completed.html')


