from django.contrib import admin
from django.forms import Textarea
from .models import Hint
from django.db import models
from .models import Test, Question, UserMetadata


class QuestionAdmin(admin.ModelAdmin):

    list_display = ('test', 'level', 'answer')
    ordering = ('level',)


class HintAdmin(admin.ModelAdmin):

    list_display = ('test', 'question', 'hint', 'is_shown')
    list_filter = ('question', 'test',)
    ordering = ('test', 'question',)


class UserMetadataAdmin(admin.ModelAdmin):

    list_display = ('user', 'test', 'questions_solved', 'time_taken',)
    list_filter = ('test',)
    ordering = ('test', '-questions_solved', 'time_taken')


admin.site.register(Test)
# admin.site.register(Question)
admin.site.register(Question, QuestionAdmin)
admin.site.register(UserMetadata, UserMetadataAdmin)
admin.site.register(Hint, HintAdmin)
