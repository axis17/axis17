from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, HttpResponse
from axis.views import event_registration_email
from axis.models import MyUser, Event, SingleParticipantEventRegistration
from axis.views import login_signup
from .models import ElectroblitzUserTestMetadata, Test, Question
from django.contrib import messages
import json
import datetime
from django.utils import timezone


def rules(request):
    if request.method == 'GET':
        return render(request, 'electroblitz/rules.html')
    elif request.method == 'POST' and ('password' in request.POST or 'password1' in request.POST):
        return login_signup(request, "electroblitz:home")
    elif request.method == 'POST' and 'PLAY' in request.POST:
        # register_for_bakerstreet(request)
        return redirect('electroblitz:home')


# @login_required(login_url='/#login')
def home(request):
    if request.user.is_anonymous:
        return redirect('electroblitz:rules')

    try:
        test = Test.objects.get(start__lte=timezone.now(), end__gte=timezone.now())
    except Test.DoesNotExist:
        return redirect('electroblitz:time_completed')

    if request.method == 'POST':
        user_metadata = ElectroblitzUserTestMetadata.objects.get(test=test, user=request.user)

        if 'submit_answer' in request.POST:
            question_id = request.POST['question_id']
            answer = str(request.POST['answer']).upper()
            user_metadata.update_answer(question_id, answer)
            messages.info(request, "Answer submitted.")
        elif 'change_question' in request.POST:
            user_metadata.save(last_question_id=int(request.POST['question_id']))

        return redirect('electroblitz:home')

    try:
        user_metadata = ElectroblitzUserTestMetadata.objects.get(user=request.user, test=test)
    except ElectroblitzUserTestMetadata.DoesNotExist:
        if (test.end - timezone.now()).total_seconds() < test.duration.total_seconds():
            return redirect('electroblitz:time_completed')
        user_metadata = ElectroblitzUserTestMetadata.objects.create(test=test, user=request.user)
        user_metadata.save()

    questions = user_metadata.test.question_set.all().order_by('id')
    last_question = Question.objects.get(id=user_metadata.last_question_id)

    attempted_question_ids = []

    for q in user_metadata.questions_answered.all().only('id'):
        attempted_question_ids.append(q.id)

    if (test.end - user_metadata.started_at) > test.duration:
        # print("current time = " + str(timezone.now()))
        # print("started at = " + str(user_metadata.started_at))
        # print("end at = " + str(test.end))
        # print("end - started = " + str((test.end - user_metadata.started_at).total_seconds() / (60 * 60)))
        # print("duration = " + str(test.duration.total_seconds()))
        # print("now - started at = " + str((timezone.now() - user_metadata.started_at).total_seconds() / (60 * 60)))
        countdown = test.duration.total_seconds() - (timezone.now() - user_metadata.started_at).total_seconds()
        # print("countdown = " + str(countdown))
    else:
        countdown = (test.end - timezone.now()).seconds

    if countdown <= 0:
        return redirect('electroblitz:time_completed')

    return render(request, 'electroblitz/home.html', context={
        'questions': questions,
        'countdown': countdown,
        'last_question': last_question,
        'attempted_question_ids': attempted_question_ids
    })


def time_completed(request):
    try:
        test = Test.objects.get(start__lte=timezone.now(), end__gte=timezone.now())
        if (test.end - timezone.now()).total_seconds() < test.duration.total_seconds():
            raise Test.DoesNotExist

        try:
            metadata = ElectroblitzUserTestMetadata.objects.get(user=request.user, test=test)
            if (timezone.now() - metadata.started_at).total_seconds() > test.duration.total_seconds():
                raise Test.DoesNotExist
        except Test.DoesNotExist:
            raise Test.DoesNotExist

        return redirect('electroblitz:home')
    except Test.DoesNotExist:
        try:
            test = Test.objects.order_by('start').get(start__gt=timezone.now())
            return render(request, 'electroblitz/time_completed.html', context={'test': test})
        except Test.DoesNotExist:
            return render(request, 'electroblitz/time_completed.html', context={})
