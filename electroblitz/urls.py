from django.conf.urls import url, include
from django.contrib import admin
# from django.contrib.sitemaps.views import sitemap
# from axis.sitemaps import *

from . import views

app_name = 'electroblitz'

urlpatterns = [

    url(r'^$', views.home, name='home'),
    url(r'^rules/$', views.rules, name='rules'),
    # url(r'^test-completed/$', views.test_completed, name='test_completed'),
    # url(r'^ajax-leaderboard/$', views.ajax_leaderboard, name='ajax_leaderboard'),
    url(r'^test-over/$', views.time_completed, name='time_completed')

]


