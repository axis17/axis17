from django.apps import AppConfig


class ElectroblitzConfig(AppConfig):
    name = 'electroblitz'
